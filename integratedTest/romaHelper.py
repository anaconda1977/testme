import sys
import os
import shutil
import platform
import datetime
import traceback 

    
class romaHelper:      
    def __init__(self, report_dir):
        self.report_dir = report_dir        
        self.last_tspath = ""
    
    #  res_path is path to the ts_name  
    def setCaseReletedInfo(self, res_path, ts_object, ind_in_g = 1, group_index=1):
        self.res_path = res_path        
        if not os.path.isdir(self.res_path):
            raise OSError("romaHelper error: Please select a test set first.")        
        self.testset = ts_object        
        self.ind_in_g = ind_in_g
        self.group_index = group_index
        
    def helper(self, module_name, para_ins):
        if module_name == "h5_composer":
            return self.h5comHelper(para_ins)        
        elif module_name == "segmentation":
            return self.segmentationHelper(para_ins)
        elif module_name == "training":
            return self.trainingHelper(para_ins)
        elif module_name == "pkl2h5":
            return self.pkl2h5Helper(para_ins)
        elif module_name == "predict":
            return self.predictHelper(para_ins)
        else:
            return None
        
        
    def checkFolderKArgs(self, input_folder):
        if not input_folder or input_folder.startswith(r"--"):
            return ""
        if os.path.isdir(input_folder):
            return input_folder
        else:
            input_tmp = os.path.join(self.res_path, input_folder)
            if os.path.isdir(input_tmp):
                return input_tmp
            else:
                return ""
                
    def checkFileKArgsKeyEndsKey(self, input_file, endsKey):
        if not input_file or input_file.startswith(r"--"):
            return ""        
        
        if os.path.isfile(input_file) and \
        (not endsKey or (endsKey and input_file.endswith(endsKey))):
            return input_file
        else:
            input_tmp = os.path.join(self.res_path, input_file)
            if os.path.isfile(input_tmp) and \
            (not endsKey or (endsKey and input_file.endswith(endsKey))):
                return input_tmp
            else:
                return ""
    
    def searchInResrc(self, bSearchDir, startKey="", middleKey="", endKey=""):
        listOfFile = os.listdir(self.res_path)
        for list_name in listOfFile:
            if startKey and not list_name.startswith( startKey ):
                continue
            if endKey and not list_name.endswith( endKey ):
                continue            
            if middleKey and (not middleKey in list_name):
                continue
                    
            name_tmp = os.path.join(self.res_path, list_name)
            if bSearchDir and os.path.isdir(name_tmp):
                return name_tmp, list_name
            if not bSearchDir and os.path.isfile(name_tmp):
                return name_tmp, list_name
        return "", ""           
        
    def modifyParas(self, paraList, strKey, strValue, indexOp, lenPara):
        if indexOp == -1:
            paraList.append(strKey)
            paraList.append(strValue)
        elif indexOp == lenPara:
            paraList.append(strValue)
        else:
            if paraList[indexOp].startswith(r"--"):
                paraList.insert(indexOp, strValue)
            else:
                paraList[indexOp] = strValue
    
    def createClearOutDir(self, para_ins, outDirKey, out_index, lenPara):
        out_dir = os.path.join( self.res_path, "output", "")
        if os.path.isdir(out_dir):
            shutil.rmtree(out_dir)
        os.mkdir(out_dir)
        
        if outDirKey.startswith(r"--") and outDirKey.endswith(r"="):
            if out_index==-1:    
                para_ins.append(outDirKey + out_dir)
            else:
                para_ins[out_index] = outDirKey + out_dir
        else:
            self.modifyParas(para_ins, outDirKey, out_dir, out_index, lenPara)
        
    def h5comHelper(self, para_ins):
        try:            
            lenPara = len(para_ins)
            wav_index = -1
            wav_root_folder = ""
            yaml_index = -1
            yaml_file = ""
            out_index = -1
            for index in range(lenPara):
                if para_ins[index] == "--input_dir":
                    wav_index = index+1
                    if wav_index < lenPara:
                        input_wav = para_ins[wav_index].strip()
                        wav_root_folder = self.checkFolderKArgs(input_wav)
                    else:
                        raise Exception("h5comHelper parameter list wrong: --input_dir")
                elif para_ins[index] == "--output_dir":
                    out_index = index+1
                elif para_ins[index] == "--config_yaml":
                    yaml_index = index + 1
                    if yaml_index < lenPara:
                        input_yaml = para_ins[yaml_index].strip()
                        yaml_file = self.checkFileKArgsKeyEndsKey(input_yaml, r".yaml")
                        if not yaml_file:
                            yaml_file = self.checkFileKArgsKeyEndsKey(input_yaml, r".yml")                        
                        if not yaml_file:
                            if self.testset and self.testset.config_fname:                        
                                input_yaml = os.path.join(self.res_path,\
                                    self.testset.config_fname)
                                if os.path.isfile(input_yaml) and \
                                    (input_yaml.endswith(r".yaml") or input_yaml.endswith(r".yml")):
                                    yaml_file = input_yaml
                                else:
                                    yaml_file = ""
                            else:
                                yaml_file = ""
                    else:
                        raise Exception("h5comHelper parameter list wrong: --config_yaml")
            
            if not wav_root_folder:
                wav_root_folder, fname = self.searchInResrc( True, "", "wav", "" )
            
            if not wav_root_folder:
                raise Exception("h5comHelper error: Wav folder not found. Select a test set or provide --input_dir augument")
            else:
                self.modifyParas(para_ins, "--input_dir", wav_root_folder, wav_index, lenPara)                
                    
            if not yaml_file:                                                             
                yaml_file, fname = self.searchInResrc( False, "", "", r".yaml" )
                if yaml_file:
                    shutil.copy(yaml_file, os.path.join(self.report_dir, fname) )
                    if self.testset:
                        self.testset.config_fname = fname
                        self.testset.save()                
            
            if not yaml_file:                    
                raise OSError("error: No yaml file found. Select a test set or provide --config_yaml string")
            else:
                self.modifyParas(para_ins, "--config_yaml", yaml_file, yaml_index, lenPara)                                
            
            #clean output folder
            self.createClearOutDir( para_ins, "--output_dir", out_index, lenPara)                           
            return para_ins    
                    
        except Exception as ex:
            raise ex
    
    def segmentationHelper(self, para_ins):
        try:
            lenPara = len(para_ins)
            input_h5_index = -1            
            out_index = -1             
            
            for index in range(lenPara):
                if para_ins[index] == "--input_h5":
                    input_h5_index = index+1                                  
                elif para_ins[index].startswith("--output_dir"):
                    out_index = index+1                    
            
            input_h5_file, fname = self.searchInResrc( False, "", "", r".h5" )                
            if not input_h5_file:
                raise OSError("segmentationHelper error: input .h5 file is not found. \
                Select a test set or provide --input_h5 string")
            else:
                self.modifyParas(para_ins, "--input_h5", input_h5_file, input_h5_index, lenPara) 
            self.createClearOutDir( para_ins, "--output_dir", out_index, lenPara)
            return para_ins   
            
        except Exception as ex:            
            #print("\nsegmentationHelper failed for some reason:", str(ex))
            raise ex    
            
    def trainingHelper(self, para_ins):
        try:
            lenPara = len(para_ins)
            train_index = -1
            testing_index = -1
            out_index = -1
            for index in range(lenPara):
                if para_ins[index] == "--h5-path-train":
                    train_index = index+1                    
                elif para_ins[index] == "--h5-path-test":
                    testing_index = index+1                    
                elif para_ins[index].startswith("--mlp-output-folder-path="):
                    out_index = index                    
                else:
                    pass
                    
            training_file, fname = self.searchInResrc( False, "", "training", r".h5" )                
            if not training_file:
                raise OSError("trainingHelper error: input training.h5 file is not found. \
                Select a test set or provide --h5-path-train string")
            else:
                self.modifyParas(para_ins, "--h5-path-train", training_file, train_index, lenPara)
            
            testing_file, fname = self.searchInResrc( False, "", "testing", r".h5" )                
            if not testing_file:
                raise OSError("trainingHelper error: input testing.h5 file is not found. \
                Select a test set or provide --h5-path-test string")
            else:
                self.modifyParas(para_ins, "--h5-path-test", testing_file, testing_index, lenPara)
                
            self.createClearOutDir( para_ins, "--mlp-output-folder-path=", out_index, lenPara)
            
            return para_ins
            
        except Exception as ex:
            print( str(traceback.print_exc()) )
            print("Training helper failed for some reason:", str(ex))
            return None

    def pkl2h5Helper(self, para_ins):
        try:
            lenPara = len(para_ins)
            inputPkl_index = -1
            yamlpath_index = -1
            out_index = -1 
            
            for index in range(lenPara):
                if para_ins[index] == "--input":
                    inputPkl_index = index+1                    
                elif para_ins[index] == "--h5_feature_config_path":
                    yamlpath_index = index+1                    
                elif para_ins[index].startswith("--output"):
                    out_index = index+1                    
                else:
                    pass
                    
            inputPkl_file, fname = self.searchInResrc( False, "", "", r".pkl" )                
            if not inputPkl_file:
                raise OSError("pkl2h5Helper error: input.pkl file is not found. \
                Select a test set or provide --input string")
            else:
                self.modifyParas(para_ins, "--input", inputPkl_file, inputPkl_index, lenPara)
                
            if not "pkl2h5" in self.res_path:
                raise OSError("pkl2h5Helper error:The pkl2h5 resource path not correct:" + self.res_path)
            pkl2h5_index = self.res_path.index("pkl2h5")
            source_yaml_path = self.res_path.replace("pkl2h5", "h5_composer")
            if not os.path.isdir(source_yaml_path):
                raise OSError("pkl2h5Helper error:The h5_composer resource path doesn't exist:"+source_yaml_path)    
            print("pkl2h5Helper source_yaml_path:"+source_yaml_path)
            print("pkl2h5Helper res_path:" + self.res_path)
            list_yaml = os.listdir(source_yaml_path)
            #copy h5_composer to here
            yaml_file = ""
            for currentfilename in list_yaml:                            
                if os.path.isfile(os.path.join(source_yaml_path, currentfilename)) and currentfilename.endswith(r".yaml"):
                    shutil.copy(os.path.join(source_yaml_path, currentfilename), \
                        os.path.join(self.res_path, currentfilename) )
                    yaml_file = os.path.join(self.res_path, currentfilename)
                    break
            if not yaml_file:
                raise OSError("pkl2h5Helper error: The yaml file doesn't exist in path:"+source_yaml_path)
            self.modifyParas(para_ins, "--h5_feature_config_path", yaml_file, \
                yamlpath_index, lenPara)
            
            self.createClearOutDir( para_ins, "--output", out_index, lenPara)            
            
            return para_ins
            
        except Exception as ex:
            print( str(traceback.print_exc()) )
            print("pkl2h5Helper failed for some reason:", str(ex))
            return None
            
    def predictHelper(self, para_ins):
        try:
            lenPara = len(para_ins)
            model_file_index = -1
            training_file_index = -1            
            audio_folder_index = -1            
            audio_folder = ""
            output_index = -1
            output_folder = ""            
            
            
            for index in range(lenPara):
                if para_ins[index] == "--model_file_name":
                    model_file_index = index+1
                elif para_ins[index] == "--training_h5_file_path":
                    training_file_index = index+1    
                elif para_ins[index] == "--output_base_folder":
                    output_index = index+1
                elif para_ins[index].startswith("--audio_file_path"):
                    audio_folder_index = index+1                    
                    if audio_folder_index < lenPara:
                        input_wav = para_ins[audio_folder_index].strip()
                        if not input_wav or input_wav.startswith(r"--"):
                            audio_folder = ""
                        if os.path.isdir(input_wav):
                            audio_folder = input_folder
                        
                else:
                    pass
                    
            model_file, fname = self.searchInResrc( False, "", "model", r".h5" )                
            if not model_file:
                raise OSError("predictHelper error: input model_file is not found. \
                Select a test set or provide --model_file_name")
            self.modifyParas(para_ins, "--model_file_name", model_file, model_file_index, lenPara)
            
            
            seg_folder = self.res_path.replace("predict", "segmentation")                        
            if not os.path.isdir(seg_folder):
                raise OSError("Error: segmentation resource folder not exist:" + seg_folder)    
            listOfFile = os.listdir(seg_folder)
            training_file = ""
            for file_name in listOfFile:
                if "training" in file_name and file_name.endswith(r".h5"):
                    training_file = os.path.join(seg_folder, file_name)
                    break 
            if not training_file:
                raise OSError("Error: training.h5 file used for is not found.")
            self.modifyParas(para_ins, "--training_h5_file_path", training_file, \
                training_file_index, lenPara)
            
            self.createClearOutDir( para_ins, "--output_base_folder", output_index, lenPara) 
            
            if not audio_folder:
                audio_folder_root = self.res_path.replace("predict", "h5_composer")
                print("audio_folder_root:" + audio_folder_root)
                listOfFile = os.listdir(audio_folder_root)
                for fname in listOfFile:
                    tmp_dir = os.path.join(audio_folder_root, fname)
                    print("tmp_dir:" + tmp_dir)
                    if os.path.isdir(tmp_dir) and "wav" in fname:
                        print( "wav dir found:" + fname )
                        audio_folder = os.path.join(audio_folder_root, fname)
                        break
            if not audio_folder:
                raise OSError("Error: audio files folder not found.")
            self.modifyParas(para_ins, "--audio_file_path", audio_folder, audio_folder_index, \
                lenPara)              
            
            return para_ins
            
        except Exception as ex:
            print( str(traceback.print_exc()) )
            print("Training helper failed for some reason:", str(ex))
            return None
    
    
    def postprocess(self, module_name):        
        if module_name == "h5_composer":
            return self.h5_composer_PP()
        elif module_name == "segmentation":
            return self.segmentation_PP()
        elif module_name == "training":
            return self.training_PP()
        elif module_name == "pkl2h5":
            return self.pkl2h5_PP()
        elif module_name == "predict":
            return self.predict_PP()
        else:
            return None
    
       
    
    def prepareForCopyResult(self, tsPathOfNextModule, nameOfNextModule):
        muxFolder = ""
        if "ts_mux" in tsPathOfNextModule:
            if self.ind_in_g == 1:
               shutil.rmtree(tsPathOfNextModule)
               os.mkdir(tsPathOfNextModule)
            else:
                pass
        else:
            if shutil.rmtree(tsPathOfNextModule):
                shutil.rmtree(tsPathOfNextModule)
            else:
                pass
            os.mkdir(tsPathOfNextModule)   
            index = tsPathOfNextModule.index(nameOfNextModule)
            muxFolder = tsPathOfNextModule[:index + len(nameOfNextModule)]
            muxFolder = os.path.join(muxFolder, "ts_mux")
            if self.ind_in_g == 1:
                if os.path.isdir(muxFolder):
                    shutil.rmtree(muxFolder)
                os.mkdir(muxFolder)                
            else:
                pass
        return muxFolder
        
    def h5_composer_PP(self):
        try:    
            if not "test_resource" in self.res_path:
                raise OSError("h5_composer_PP:" + self.res_path + \
                " is not a resource path")
            if not "h5_composer" in self.res_path:
                raise OSError("h5_composer_PP:" + self.res_path + " \
                is not a h5_composer resource path")
            
            #copy output h5 files to segmentation resource folder for next step                        
            h5comOutputFolder = os.path.join(self.res_path, "output", "")
            if not os.path.isdir(h5comOutputFolder):
                raise OSError("h5_composer_PP:"+h5comOutputFolder + " is not a valid folder.")
            
            segResrcFolder = self.res_path.replace("h5_composer", "segmentation")
            
            segParentFolder = os.path.dirname(segResrcFolder)
            print("segParentFolder:",segParentFolder)
            if not os.path.isdir(segParentFolder):
                os.mkdir(segParentFolder) 
            
            muxFolder = self.prepareForCopyResult(segResrcFolder, "segmentation")
            listOfFile = os.listdir(h5comOutputFolder)
            succeedTag = False
            for file_name in listOfFile:
                if ("testing" in file_name or "training" in file_name) and file_name.endswith("h5"):                
                    succeedTag = True
                self.copy_result(h5comOutputFolder, file_name, segResrcFolder) 
                self.copy_result(h5comOutputFolder, file_name, self.report_dir, "h5_composer" )
                if muxFolder and os.listdir(muxFolder):
                    self.copy_result(h5comOutputFolder, file_name, muxFolder )
                    
            if not succeedTag:
                raise(OSError(r"h5 file not found in "+h5comOutputFolder))
                
        except Exception as ex:
            print(str(traceback.print_exc()))
            print("h5_composer_PP failed for some reason:", str(ex))
    
    def segmentation_PP(self):
        try:
            if not "test_resource" in self.res_path:
                raise OSError("segmentation_PP:" + self.res_path+" is not a resource path")
            if not "segmentation" in self.res_path:
                raise OSError("segmentation_PP:" + self.res_path + \
                    " is not a segmentation resource path")                
            
            segOutputFolder = os.path.join(self.res_path, "output", "")
            if not os.path.isdir(segOutputFolder):
                msg = r"segmentation_PP: Segmentation output folder not found:" \
                + segOutputFolder + "\n"                    
                raise(OSError(msg)) 
                
            trainingResFolder = self.res_path.replace("segmentation", "training") 
            trParentFolder = os.path.dirname(trainingResFolder)
            print("segmentation_PP: training Parent Folder:", trParentFolder)
            if not os.path.isdir(trParentFolder):
                os.mkdir(trParentFolder) 
                
            muxFolder = self.prepareForCopyResult(trainingResFolder, "training")
                    
            listOfFile = os.listdir(segOutputFolder)
            succeedTag = False
            for file_name in listOfFile:
                if ("testing" in file_name or "training" in file_name) and file_name.endswith("h5"):    
                    succeedTag = True                                               
                self.copy_result(segOutputFolder, file_name, trainingResFolder) 
                self.copy_result(segOutputFolder, file_name, self.report_dir, "segmentation" )
                if muxFolder and os.listdir(muxFolder):
                    self.copy_result(segOutputFolder, file_name, muxFolder )
                    
            if not succeedTag:
                raise(OSError(r"h5 file not found in folder:"+segOutputFolder))
            
        except Exception as ex:
            print(str(traceback.print_exc()))
            print("segmentation_PP failed for some reason:", str(ex))

    def training_PP(self):
        try:
            if not "test_resource" in self.res_path:
                raise OSError(self.res_path + " is not a resource path")
            if not "training" in self.res_path:
                raise OSError(self.res_path + " is not a training resource path")
                
            trainingOutputFolder = os.path.join(self.res_path, "output", "")
            if not os.path.isdir(trainingOutputFolder):
                msg = r"training_PP: Training output folder not found:" \
                + trainingOutputFolder + "\n"                    
                raise(OSError(msg)) 
                
            pkl2h5ResFolder = self.res_path.replace("training", "pkl2h5") 
            pkl2h5ParentFolder = os.path.dirname(pkl2h5ResFolder)
            print("training_PP: pkl2h5 Parent Folder:", pkl2h5ParentFolder)
            if not os.path.isdir(pkl2h5ParentFolder):
                os.mkdir(pkl2h5ParentFolder)
                
            muxFolder = self.prepareForCopyResult(pkl2h5ResFolder, "pkl2h5")
            
            listOfFile = os.listdir(trainingOutputFolder)
            succeedTag = False
            for file_name in listOfFile:
                if file_name.endswith(r".pkl"):
                    succeedTag = True
                self.copy_result(trainingOutputFolder, file_name, pkl2h5ResFolder) 
                self.copy_result(trainingOutputFolder, file_name, self.report_dir, "training" )
                if muxFolder and os.listdir(muxFolder):
                    self.copy_result(trainingOutputFolder, file_name, muxFolder ) 
                    
            if not succeedTag:
                raise(OSError(r"pkl file not found in folder:"+trainingOutputFolder))
            
        except Exception as ex:
            print(str(traceback.print_exc()))
            print("training_PP failed for some reason:", str(ex))

    def pkl2h5_PP(self):
        try:
            if not "test_resource" in self.res_path:
                raise OSError(self.res_path + " is not a resource path")
            if not "pkl2h5" in self.res_path:
                raise OSError(self.res_path + " is not a pkl2h5 resource path")                  
            
            pkl2h5OutputFolder = os.path.join(self.res_path, "output", "")
            if not os.path.isdir(pkl2h5OutputFolder):
                msg = r"pkl2h5_PP: pkl2h5 output folder not found:" \
                + pkl2h5OutputFolder + "\n"                    
                raise(OSError(msg)) 
                
            predictResFolder = self.res_path.replace("pkl2h5", "predict") 
            predictParentFolder = os.path.dirname(predictResFolder)
            print("pkl2h5_PP: predictParentFolder:", predictParentFolder)
            if not os.path.isdir(predictParentFolder):
                os.mkdir(predictParentFolder)
                    
            muxFolder = self.prepareForCopyResult(predictResFolder, "predict")
            
            listOfFile = os.listdir(pkl2h5OutputFolder)
            succeedTag = False
            for file_name in listOfFile:
                if file_name.endswith(r".h5"):
                    succeedTag = True
                self.copy_result(pkl2h5OutputFolder, file_name, predictResFolder) 
                self.copy_result(pkl2h5OutputFolder, file_name, self.report_dir, "pkl2h5" )
                if muxFolder and os.listdir(muxFolder):
                    self.copy_result(pkl2h5OutputFolder, file_name, muxFolder )    
            if not succeedTag:
                raise(OSError(r"Converted H5 file not found in folder:"+pkl2h5OutputFolder))
            
        except Exception as ex:
            print(str(traceback.print_exc()))
            print("pkl2h5_PP failed for some reason:", str(ex))

    def predict_PP(self):
        try:
            if not "test_resource" in self.res_path:
                raise OSError(self.res_path + " is not a resource path")
            if not "predict" in self.res_path:
                raise OSError(self.res_path + " is not a predict resource path")
            
            predictOutputFolder = os.path.join(self.res_path, "output", "")
            if not os.path.isdir(predictOutputFolder):
                msg = r"predict_PP: predict output folder not found:" + predictOutputFolder + "\n"
                raise(OSError(msg))
            
            listOfFile = os.listdir(predictOutputFolder)            
            for file_name in listOfFile:            
                self.copy_result(predictOutputFolder, file_name, self.report_dir, "predict")
                
        except Exception as ex:
            print(str(traceback.print_exc()))
            print("predict_PP failed for some reason:", str(ex))
    
    def copy_result(self, scrRoot, fname, destRoot, module_name = ""):
        scrName = os.path.join(scrRoot, fname)
        if module_name:
            destRoot = os.path.join(destRoot, module_name + "_group_" + \
                str(self.group_index) + "_case_" + str(self.ind_in_g) )
            if not os.path.isdir(destRoot):
                os.mkdir(destRoot)            
        
        if os.path.isfile( scrName ):
            shutil.copy(scrName, os.path.join(destRoot, fname) )
        elif os.path.isdir( scrName ):
            shutil.copytree(scrName, os.path.join(destRoot, fname) )
        else:
            pass
        
        
if __name__ == "__main__":   
    romaHelper.h5_composer_postprocessing(r"D:\project\test_root\test_resource\ROMA\integrated_test\h5_composer\test")

