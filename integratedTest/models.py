from django.db import models
from django.db.models import UniqueConstraint
from django.core.validators import validate_comma_separated_integer_list
import datetime
from django.utils import timezone

PROJNAME_CHOICES = (
        ('ROMA', 'ROMA'), 
        ('PDK','PDK'),
        ('PMIC','PMIC'),
        ('OTHER','OTHER')
)

TEST_TYPE_CHOICES = (
        ('integrated_test', 'integrated_test'), 
        ('system_test','system_test'),           
        ('API_test', 'API_test'),
        ('module_test', 'module_test'), 
        ('selenium_test', 'selenium_test'),
        ('for_demo', 'for_demo'),
        ('others', 'others'),
)

MODULE_CHOICES = (        
        ('h5_composer','h5_composer'),           
        ('training', 'training'),
        ('pkl2h5', 'pkl2h5'),
        ('predict', 'predict'), 
        ('segmentation', 'segmentation'),
        ('augmentation', 'augmentation'),
        ('ppc', 'ppc'),
        ('root','root'),
        ('roma', 'roma'),    
        ('None', 'None')
) 

class Projects(models.Model):
    name = models.CharField(max_length=100, unique=True)
    module_list = models.CharField(max_length=1023, blank=True)
    created_by = models.CharField(max_length=100, default = "Wason")     
    create_datetime = models.DateTimeField("Created on", auto_now = True)    

class TestCase(models.Model):
    EXECUTABLE_TYPE_CHOICES = (
        ('py_script', 'py_script'),
        ('shell', 'shell'),
        ('others', 'others'),
    )       
    name = models.CharField(max_length=200, unique=True)
    executable = models.CharField(max_length=200, default = "")
    parameter_list = models.TextField(blank=True, default = "")
    executable_type = models.CharField(max_length=100, choices = EXECUTABLE_TYPE_CHOICES, default = "py_script")    
    project_name = models.CharField(max_length=100)
    module_name = models.CharField(max_length=100)    
    created_by = models.CharField(max_length=100, default = "")     
    create_datetime = models.DateTimeField("testcase created on", auto_now = True)
    def save(self, *args, **kwargs):
        print("\n\n\nTestCase self(new) :")
        print(self.name, " ", self.executable, " ", self.parameter_list)
        print(self.executable_type, " ", self.project_name, " ", self.module_name)
        if self.pk:
           obj = TestCase.objects.get(pk=self.pk)
           print("\n\n\nTestCase obj(old) :")
           print(obj.name, " ", obj.executable, " ", obj.parameter_list)
           print(obj.executable_type, " ", obj.project_name, " ", obj.module_name)           
           if not self.executable and obj.executable:
               self.executable = obj.executable
        
        super().save(*args, **kwargs)
    class Meta:
        ordering = ['-create_datetime']
        
class TestSuite(models.Model):    
    name = models.CharField(max_length=200, unique=True)
    test_type = models.CharField(max_length=200, choices = TEST_TYPE_CHOICES, default = "integrated_test") 
    test_case_list = models.CharField(max_length=200, default = "") 
    project_name = models.CharField(max_length=100, default="OTHER")
    created_by = models.CharField(max_length=100, default = "Wason")     
    create_datetime = models.DateTimeField("TestSuite created on", auto_now = True)
    def cases_as_list(self):
        return self.test_case_list.split(',')
    class Meta:
        ordering = ['-create_datetime']

def validate_progress(value):
    if value < 0 or value > 100:
        raise ValidationError(
            _('%(value)s not a valid progress value [0, 100]'),
            params={'value': value},
        )
        
class TestInstance(models.Model):
    name = models.CharField(max_length=200, unique=True)    
    testsuite = models.CharField(max_length=200)
    test_status = models.CharField(max_length=200, default = "Not Run")
    progress = models.IntegerField(default = 0, validators=[validate_progress])    
    test_result = models.CharField(blank=True, max_length=200, default = "N/A")
    command_list = models.CharField(blank=True, max_length=2048, default = "")
    created_from_web = models.BooleanField(default = True)
    created_by = models.CharField(max_length=100, default = "Wason")
    last_touched = models.DateTimeField("last run date", default = timezone.now)
    last_run = models.DateTimeField("last run date", null=True)    
    create_datetime = models.DateTimeField("instance created on", auto_now_add = True)
    class Meta:
        ordering = ['-last_run', '-create_datetime']

            
class TestReport(models.Model):
    name = models.CharField(max_length=200, unique=True)    
    testinstance = models.ForeignKey(TestInstance, on_delete=models.CASCADE)
    test_detail = models.CharField(blank=True, max_length=1023, default = "")    
    test_result = models.CharField(blank=True, max_length=200, default = "Pass")
    log_path = models.CharField(blank=True, max_length=1023, default = "")
    create_datetime = models.DateTimeField("instance created on", auto_now = True)
    def detail_as_list(self):
        return self.test_detail.split(',')
    class Meta:
        ordering = ['-create_datetime']
        
class ActiveTestSet(models.Model):       
    name = models.CharField(max_length=200)
    instance_id = models.IntegerField(default = -1)
    case_index_in_instance = models.IntegerField(default = -1)
    full_path = models.CharField(max_length=500)
    config_fname = models.CharField(max_length=200, default = "")
    

#Remember last used things
class RecentlyUsedSetting(models.Model):       
    project_name = models.CharField(max_length=100, default = "")
    repo_path = models.CharField(max_length=511, default = "")
    path_sel = models.CharField(max_length=511, default = "")
    exe_sel= models.CharField(max_length=128, default = "")
    create_datetime = models.DateTimeField(auto_now = True)

#Used for supervise whether one instance is running.
#Fake run is permitted when a true run isn't finished.
class RunningTable(models.Model):
    class Meta:
        UniqueConstraint(fields = ['instance_id', 'is_true_run'], name = 'constraint_name')
    instance_id = models.IntegerField()
    project_name = models.CharField(max_length=100, default = "ROMA")
    is_running = models.BooleanField(default = False)
    is_true_run = models.BooleanField(default = True)
    start_datetime = models.DateTimeField(null=True, blank=True)
    complete_datetime = models.DateTimeField(null=True, blank=True)
    fake_run_result = models.CharField(max_length=1024, default = "")
    create_datetime = models.DateTimeField(auto_now = True)

    
class GlobalSetting(models.Model):       
    name = models.CharField(max_length=200, default="noname", unique=True)
    hostname = models.CharField(max_length=200, default="unknownhost")
    ostype = models.CharField(max_length=200, default="unknownos")
    project_name = models.CharField(max_length=100)
    is_active = models.BooleanField(default = False)
    user = models.CharField(max_length=100, default = "")
    #def save(self, *args, **kwargs):
        #if self.__class__.objects.count():
            #self.pk = self.__class__.objects.first().pk
        #super().save(*args, **kwargs)

 
class Repository(models.Model):
    project_name = models.CharField(max_length=200, default="")
    module_name = models.CharField(max_length=100, default="None")
    gitserver_msg = models.CharField(max_length=200, default="")
    branch_msg = models.CharField(max_length=200, default="")
    revision_hash = models.CharField(max_length=200, default="")    
    create_datetime = models.DateTimeField(auto_now = True)

def upload_to(instance, filename):
    #print(instance.project_name+"/"+filename)
    return (instance.project_name+"/"+filename) 
    
class Document(models.Model):
    #docfile = models.FileField(upload_to='documents/%Y/%m/%d')
    docfile = models.FileField(upload_to=upload_to)
    project_name = models.CharField(max_length=100)
    

def get_objs_else_None(modelName, **kwargs):
    try:
        return modelName.objects.filter(**kwargs)
    except modelName.DoesNotExist:
        return None

def get_obj_else_None(modelName, **kwargs):
    try:
        return modelName.objects.get(**kwargs)
    except modelName.DoesNotExist:
        return None    