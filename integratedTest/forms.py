from .models import TestCase, GlobalSetting, TestSuite, TestReport, TestInstance, PROJNAME_CHOICES, Projects
from django import forms


class TestCaseUpdateForm(forms.ModelForm):
    class Meta:
        model = TestCase
        fields = ( 'name', 'executable', 'parameter_list', 'executable_type', 'project_name', 'module_name', 'created_by')
        

class TestInstanceForm(forms.ModelForm):
    class Meta:
        model = TestInstance
        fields = ( 'name', 'testsuite', 'created_by', 'created_from_web')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'mytextinputclasssmall'}),            
            'created_by': forms.TextInput(attrs={'class': 'mytextinputclasssmall'}),
        }
        
class GlobalSetUpdateForm(forms.ModelForm):
    class Meta:
        model = GlobalSetting
        fields = ( 'name', 'hostname', 'ostype', 'user', 'is_active', 'project_name' )

class TestSuiteForm(forms.ModelForm):
    class Meta:
        model = TestSuite
        fields = ( 'name', 'test_case_list', 'project_name', 'test_type', 'created_by' )

class TestReportForm(forms.ModelForm):
    class Meta:
        model = TestReport
        fields = ( 'name', 'test_detail', 'test_result', 'log_path' )

class ProjectsForm(forms.ModelForm):
    class Meta:
        model = Projects
        fields = ( 'name', 'module_list', 'created_by' )
        
class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    file = forms.FileField()
    
class DocumentForm(forms.Form):
    project_name = forms.CharField(
        label='Please select a project name for your script', 
        widget=forms.Select(choices=PROJNAME_CHOICES)
    )
        
    docfile = forms.FileField(
        label='Select a file',
        help_text='max. 42 megabytes'
    )


    
    