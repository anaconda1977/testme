from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import TestCase, TestInstance, TestSuite, GlobalSetting, TestReport, RunningTable, Projects
from .models import PROJNAME_CHOICES, Document, Repository, get_objs_else_None, get_obj_else_None
from .models import TEST_TYPE_CHOICES, MODULE_CHOICES, ActiveTestSet, RecentlyUsedSetting
from .forms import TestCaseUpdateForm, GlobalSetUpdateForm, TestSuiteForm, TestReportForm, TestInstanceForm, UploadFileForm, DocumentForm, ProjectsForm
import logging
logger = logging.getLogger(__name__)

import datetime
import sys
import shutil
import threading
import _thread
import shlex
import json
import pytz
from tzlocal import get_localzone
from django.conf import settings


import platform
import subprocess
import time
import os
import traceback
from django.utils import timezone
from django.db import connection
from .reportHelper import reportHelper
from .logHelper import logHelper
from .jiraHelper import JiraHandler
from .folderHelper import folderHelper
from .runningHelper import runningHelper
from .repositoryHelper import repositoryHelper
from .testCaseHelper import testCaseHelper
from .folderBrowser import folderBrowser
from .resultAnalyzer import resultAnalyzer
from .defaultsHelper import defaultsHelper
from .emailHelper import * 
from . import initTestMe

from apscheduler.schedulers.background import BackgroundScheduler
from django_apscheduler.jobstores import register_events, register_job, DjangoJobStore
from apscheduler.triggers import cron
# Create scheduler to run in a thread inside the application process
#logger.info("settings.SCHEDULER_CONFIG", str(settings.SCHEDULER_CONFIG))
#myScheduler = BackgroundScheduler(settings.SCHEDULER_CONFIG)
myScheduler = BackgroundScheduler()
myScheduler.add_jobstore(DjangoJobStore(), "default")

# Adding this job here instead of to crons.
# This will do the following:
# - Add a scheduled job to the job store on application initialization
# - The job will execute a model class method at midnight each day
# - replace_existing in combination with the unique ID prevents duplicate copies of the job
#myScheduler.add_job("core.models.MyModel.my_class_method", "cron", id="my_class_method", hour=0, replace_existing=True)

# Add the scheduled jobs to the Django admin interface
register_events(myScheduler)

intTemplatePath = "integratedTest"
navigatorPath = os.path.join("integratedTest", "navigator")
if not logger:
    print("Invalid logger!")
    sys.exit(-1)
    
my_defaults = defaultsHelper(os.path.join(intTemplatePath, "defaults.json"), logger)
my_folderHelper = None
initialized = False    
if os.path.isfile(os.path.join(intTemplatePath, "initialized.txt")):
    logger.info("initialized.txt found.")
    initialized = True
    myScheduler.start()
    result = initTestMe.init(my_defaults, intTemplatePath, logger)
    my_folderHelper = folderHelper(logger)
    if not result:
        sys.exit(-1)
    inses = get_objs_else_None(RunningTable, is_running=True)
    for ins in inses:        
        ins.delete()
else:
    logger.info(os.getcwd())


OS_slash = "\\"
OS = "Windows"
if platform.system() == "Windows": 
    OS_slash = "\\"    
else:
    OS_slash = "/"
    OS = "Linux"

my_fldr_brwsr = folderBrowser(logger)
my_analyzer = resultAnalyzer(logger)
myTestcaseHelper = testCaseHelper(logger)
if initialized and (not my_folderHelper or not my_fldr_brwsr or not my_analyzer or \
    not my_defaults or not myTestcaseHelper):
    logger.error("Init error!")
    sys.exit(-1)

def getTestcaseByInstance(instance):
    instance.last_touched=timezone.now()
    instance.save()
    testsuite_name = instance.testsuite
    testsuite_objs = TestSuite.objects.filter(name=testsuite_name)
    if not testsuite_objs or not testsuite_objs[0]:
        logger.info("testsuite_name=" + str(testsuite_name))
        raise Exception("Test suite not found with name=" + str(testsuite_name))
        
    testsuite_obj = testsuite_objs[0]
    test_case_str_list = testsuite_obj.test_case_list.split(",")
    #logger.info("test_case_str_list:" + str(test_case_str_list)) 
    return test_case_str_list, testsuite_obj, "suceeded" 
    
def getTestcaseByInstanceID(instance_id):
    instance = get_obj_else_None(TestInstance, pk=instance_id) 
    logger.info("instance_id=" + str(instance_id))
    for key, value in instance.__dict__.items():
        logger.info("key=" + str(key) + ";value=" + str(value))
    if not instance:
        raise Exception("Test instance not found with id=" + str(instance_id))
    test_case_str_list, testsuite_obj, msg = getTestcaseByInstance(instance)
    return instance, test_case_str_list, testsuite_obj, msg 
    
    
def testCase(request, context={}):
    template_name = os.path.join(navigatorPath, 'testcase.html')
    cases = TestCase.objects.all()    
    logger.info("testCase:" + str(testCase))    
    return render(request, template_name, {"TestCase_list": cases, "context":context})
    
        
class projectsView(ListView):
    template_name = os.path.join(navigatorPath, 'projects.html')
    context_object_name = 'proj_list'
    model = Projects
    ordering = ['-create_datetime']       
    
class AddProjectView(CreateView):
    model = Projects
    form_class = ProjectsForm
    
    def get_success_url(self):
        return reverse("integratedTest:project")
        
def addCaseSubmit(request):
    context = {}
    try:
        name = ""
        project_name = ""
        module_name = ""
        executable_type = ""
        executable = ""
        parameter_list = ""
        created_by = ""
        for key, value in request.POST.items():
            logger.info(key + "=" + str(value))
        if "name" in request.POST:
            name = request.POST["name"]
        if "project_name" in request.POST:
            project_name = request.POST["project_name"]    
        if "module_name" in request.POST:
            module_name = request.POST["module_name"]    
        if "executable_type" in request.POST:
            executable_type = request.POST["executable_type"]    
        if "executable" in request.POST:
            executable = request.POST["executable"]
        if "parameter_list" in request.POST:
            parameter_list = request.POST["parameter_list"]
        if "created_by" in request.POST:
            created_by = request.POST["created_by"]
        new_obj = TestCase(name = name, project_name = project_name, 
            module_name = module_name, executable_type = executable_type, 
            executable = executable, parameter_list = parameter_list, created_by = created_by)
            
        new_obj.save()    
            
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
        context['error'] = err_msg
      
    return testCase(request, context)

def editCaseSubmit(request, pk):
    context = {}
    try:
        name = ""
        project_name = ""
        module_name = ""
        executable_type = ""
        executable = ""
        parameter_list = ""
        created_by = ""
        for key, value in request.POST.items():
            logger.info(key + "=" + str(value))
        if "name" in request.POST:
            name = request.POST["name"]
        if "project_name" in request.POST:
            project_name = request.POST["project_name"]    
        if "module_name" in request.POST:
            module_name = request.POST["module_name"]    
        if "executable_type" in request.POST:
            executable_type = request.POST["executable_type"]    
        if "executable" in request.POST:
            executable = request.POST["executable"]
        if "parameter_list" in request.POST:
            parameter_list = request.POST["parameter_list"]
        if "created_by" in request.POST:
            created_by = request.POST["created_by"]
        
        this_case = get_obj_else_None(TestCase, pk=pk)
        if this_case:
            #Suite test_case_list field may be affected by changing name
            if this_case.name != name:
                all_suites = TestSuite.objects.all()
                for one_suite in all_suites:
                    if not this_case.name in one_suite.test_case_list:
                        continue
                    case_list = one_suite.test_case_list.split(",")
                    new_case_list = []
                    for case_str in case_list:
                        if not this_case.name in case_str or \
                            not "*" in case_str:
                            new_case_list.append(case_str)
                            continue
                        case_name_group = case_str.split("*")[0]
                        if not "$" in case_str:
                            if this_case.name != case_name_group:
                                new_case_list.append(case_str)
                                continue
                            else:
                                new_name = "*".join([name, case_str.split("*")[1]])
                                new_case_list.append(new_name)
                        else:
                            name_group_splits = case_name_group.split("$")
                            if this_case.name != name_group_splits[1]:
                                new_case_list.append(case_str)
                                continue
                            else:
                                new_name = "$".join([name_group_splits[0], name])
                                new_name = "*".join([new_name, case_str.split("*")[1]])
                                new_case_list.append(new_name)
                    #endfor
                    one_suite.test_case_list = ",".join(new_case_list)
                    one_suite.save()
            
            this_case.name = name
            this_case.project_name = project_name
            this_case.module_name = module_name
            this_case.executable_type = executable_type
            this_case.executable = executable
            this_case.parameter_list = parameter_list
            this_case.created_by = created_by            
            this_case.save()
        else:
            raise(Exception("TestCase not found: pk=" + str(pk) ))
            
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
        context['error'] = err_msg
      
    return testCase(request, context)
    
def editCaseHelp(request, context = {}, this_case = None):
    if not request:
        raise(Exception("request is empty."))
    exe_types = []    
    
    for key, value in request.POST.items():
        logger.info(key + "=" + str(value))
    name_clk = ""        
    clk_type = ""
    casename = ""
    cur_mod = ""
    cur_plist = ""
    cur_proj = ""
    cur_etype = ""
    cur_crt_by = ""
    if "clk_type" in request.POST:
        clk_type = request.POST["clk_type"].strip()
    if "cur_proj" in request.POST:
        cur_proj = request.POST["cur_proj"].strip()
    
    if "name_clk" in request.POST:
        name_clk = request.POST["name_clk"].strip()
    else:
        logger.info("name_clk not here")
    if "casename" in request.POST:
        casename = request.POST["casename"].strip()
    #current module selected, in which current means may change before submitting
    if "cur_mod" in request.POST:
        cur_mod = request.POST["cur_mod"].strip()
    #current parameter list, in which current means may change before submitting
    if "cur_plist" in request.POST:
        cur_plist = request.POST["cur_plist"].strip()
    if "cur_etype" in request.POST:
        cur_etype = request.POST["cur_etype"].strip()    
    if "cur_crt_by" in request.POST:
        cur_crt_by = request.POST["cur_crt_by"].strip()
    
    #handle executable_type, created_by
    for key, value in TestCase.EXECUTABLE_TYPE_CHOICES:
        exe_types.append(value)
    context['etypes'] = exe_types
    if not clk_type:
        if this_case:
            context['ename_0'] = this_case.executable_type
            context['cur_crt_by'] = this_case.created_by
        else:
            context['ename_0'] = exe_types[0]
    else:
        if cur_etype:
            context['ename_0'] = cur_etype
        else:
            context['ename_0'] = exe_types[0]
        if cur_crt_by:
            context['cur_crt_by'] = cur_crt_by
    
    #LastUsed remembers path or other things in the last action the user used
    LastUsed = get_obj_else_None(RecentlyUsedSetting)
    if LastUsed:
        logger.info("LastUsed: found" + LastUsed.repo_path)
    else:
        LastUsed = RecentlyUsedSetting( project_name = "", repo_path = "")
        LastUsed.save()
    
    context['MODULE_0'] = ""
    if clk_type:
        if cur_proj:
            LastUsed.project_name = cur_proj
        if cur_mod:
            context['MODULE_0'] = cur_mod
    else:
        if this_case:
            LastUsed.project_name = this_case.project_name
            context['MODULE_0'] = this_case.module_name
    
    
    #get existing projects and their modules
    proj_list = {}
    proj_obs = Projects.objects.all()
    if not proj_obs or len(proj_obs) == 0:
        err = "Add your first project please."
        raise(Exception(err))
    for item in proj_obs:
        mod_list = item.module_list.split()
        proj_list[item.name] = mod_list
        logger.info(item.name + ":" + str(mod_list))
        if not LastUsed.project_name:
            LastUsed.project_name = item.name 
        if LastUsed.project_name==item.name:
            context['mod_list'] = mod_list
            if not context['MODULE_0']:
                context['MODULE_0'] = mod_list[0]
            
    logger.info("LastUsed.project_name:" + LastUsed.project_name) 
    repo_path = my_folderHelper.get_reposit(LastUsed.project_name)
    
    if cur_mod == "ppc":
        repo_path = os.path.join( repo_path, "ppc")
        if not os.path.isdir(repo_path):
            os.mkdir(repo_path)
           
    #handle folder browser
    if not os.path.isdir(LastUsed.repo_path):
        LastUsed.repo_path = repo_path
    LastUsed.save()
     
    if not clk_type:                 
        logger.info("Create new case.")
        LastUsed.path_sel = ""
        LastUsed.exe_sel = ""
        LastUsed.save()
        
    #update LastUsed.repo_path based on click, which is path to current folder to display.        
    if clk_type == "dir" and name_clk:            
        logger.info("LastUsed.project_name" + LastUsed.project_name)                    
        if name_clk==r"..":
            if not cur_mod == "ppc":
                if not LastUsed.repo_path.endswith("test_repo"):
                    LastUsed.repo_path = os.path.dirname(LastUsed.repo_path)
            else:
                if not LastUsed.repo_path.endswith("test_scripts"):
                    LastUsed.repo_path = os.path.dirname(LastUsed.repo_path)
        else:
            path_to_check = os.path.join(LastUsed.repo_path, name_clk)
            if os.path.isdir(path_to_check): 
                LastUsed.repo_path = path_to_check
            else:
                pass                    
        LastUsed.save()
    logger.info("trace")    
    listOfFile = os.listdir(LastUsed.repo_path)
    dirlist = []
    exefilelist = []
    unexefilelist = []
    if not LastUsed.repo_path.endswith("test_repo"):
        dirlist.append(r"..")
    for fname in listOfFile:
        myfile = os.path.join(LastUsed.repo_path, fname)
        if os.path.isfile(myfile):
            if fname.endswith(r".py") or fname.endswith(r".sh"):
                exefilelist.append(fname) 
            else:
                unexefilelist.append(fname) 
        else:                
            dirlist.append(fname)
    context['dirs'] = dirlist
    context['exefiles'] = exefilelist
    context['unexefiles'] = unexefilelist
    if clk_type == "file":        
        file_to_check = os.path.join(LastUsed.repo_path, name_clk)
        #When you have select a exe file, even not submit yet, the dir/name of THE SELECTED
        #will be saved in LastUsed.path_sel/LastUsed.exe_sel as a pair. Even you browse
        #to other folder, you won't lost your temp SELECTED until you click on a new file.
        if name_clk and os.path.isfile(file_to_check):
            LastUsed.path_sel = LastUsed.repo_path
            LastUsed.exe_sel = name_clk
            LastUsed.save()            
            context['fileClked'] = name_clk
            context['dirSled'] = LastUsed.repo_path
    
    else:
        context['fileClked'] = ""
        #When comes to the folder of THE temp SELECTED, THE SELECTED file will be marked 
        #with a tick. "fileClked" contains basename. The logic is as below:
        if LastUsed.repo_path == LastUsed.path_sel:
            context['fileClked'] = LastUsed.exe_sel        
        
    if "test_repo" in LastUsed.path_sel:
        index_root = LastUsed.path_sel.rindex("test_repo")
        root_dir = LastUsed.path_sel[index_root+10:]
        context['fileSled'] = os.path.join(root_dir, LastUsed.exe_sel)
    
    #handle parameter list
    temp_para_all = my_defaults.getParaInitial()
    if LastUsed.project_name in temp_para_all:
        para_thisProj = temp_para_all[LastUsed.project_name]
        if context['MODULE_0'] in para_thisProj:
            context['para_str'] = para_thisProj[context['MODULE_0']]
        
    if this_case and not clk_type:
        casename = this_case.name
        context['para_str'] = this_case.parameter_list
    #end handle parameter list
    if this_case:
        context['my_case_id'] = this_case.id
    context['casename'] = casename
    context['PROJS'] = proj_list
    context['PROJS_0'] = LastUsed.project_name
    
    logger.info("PROJS\n" + str(context['PROJS']))
    logger.info(context['PROJS_0'])
    logger.info(context['MODULE_0'])
    
'''
Note 2 senarios may come to here:
1. create a blank case page. 
2. edit selection of the new case before submission.
'''   
def addTestCase(request):
    context = {}
    try:
        editCaseHelp(request, context, None)
        #logger.info("context:" + str(context))        
        return render(request, os.path.join(intTemplatePath, "testcase_form.html"), context)
        
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
        context['error'] = err_msg
        
    return render(request, os.path.join(navigatorPath, "testcase.html"), context)


'''
Note 2 senarios may come to here:
1. create a blank case page. 
2. edit selection of the case before submission.
'''
def editCase(request, pk):  
    context = {}
    try:
        this_case = get_obj_else_None(TestCase, pk=pk)
        if not this_case:
            raise(Exception("Case not found with pk=" + str(pk)))
        editCaseHelp(request, context, this_case)
        return render(request, os.path.join(intTemplatePath, "testcase_update_form.html"), context)
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
        context['error'] = err_msg
        
    return render(request, os.path.join(navigatorPath, "testcase.html"), context)

def addInstanceSubmit(request):
    context = {}
    try:
        name = ""
        testsuite = ""        
        from_web = False
        created_by = ""
        for key, value in request.POST.items():
            logger.info(key + "=" + str(value))
        if "name" in request.POST:
            name = request.POST["name"]
        if "testsuite" in request.POST:
            testsuite = request.POST["testsuite"]    
        if "from_web" in request.POST:
            from_web = request.POST["from_web"]    
        if "created_by" in request.POST:
            created_by = request.POST["created_by"]    
        
        new_obj = TestInstance(name = name, testsuite = testsuite, 
            created_from_web = from_web, created_by = created_by)
            
        new_obj.save()    
            
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
        context['error'] = err_msg
      
    return index(request, **context)
    
def addInstance(request):
    context = {}
    try:
        my_suite_list = TestSuite.objects.all()        
        suite_namelist = []
        if not my_suite_list:
            raise(Exception("No test suite exists. Please create one in Suite tab or import from Standard Test Library(testMeSTL)."))
        for my_suite in my_suite_list:             
            for key, value in my_suite.__dict__.items():
                logger.info(str(key) + "=" + str(value))
                if key=="name":
                    suite_namelist.append(value)
        context['suite_names'] = suite_namelist
        context['from_web'] = True
        return render(request, os.path.join(intTemplatePath, "testinstance_form.html"), {"context": context})
        
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
        context['error'] = err_msg
        
    return render(request, os.path.join(navigatorPath, "index.html"), **{"context": context})
    
    

class AddInstanceForCICDView(CreateView):
    model = TestInstance
    form_class = TestInstanceForm
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        my_suite_list = TestSuite.objects.all()
        suite_namelist = []
        logger.info("AddInstanceForCICDView")
        for my_suite in my_suite_list:             
            for key, value in my_suite.__dict__.items():
                logger.info(str(key) + "=" + str(value))
                if key=="name":
                    suite_namelist.append(value)
        context['suite_names'] = suite_namelist
        context['from_web'] = False
        return context
        
    def form_valid(self, form):
        form.cleaned_data['created_from_web'] = False
        self.object = form.save()
        self.object.created_from_web = False        
        return super().form_valid(form) 
        
    def get_success_url(self):
        return reverse("integratedTest:instance_cicd")

def testSuite(request, context={}):
    template_name = os.path.join(navigatorPath, 'testsuite.html')
    suites = get_objs_else_None(TestSuite)
    cases = get_objs_else_None(TestCase)
    context["test_suite_list"] = suites
    context["testcases"] = cases
    return render(request, template_name, context)

        
def suiteSubmit(request):
    context = {}
    try:
        name = ""
        project_name = ""
        test_type = ""
        test_case_list = ""
        created_by = ""        
        suite_id = None
        
        for key, value in request.POST.items():
            logger.info(key + "=" + str(value))
        if "suite_id" in request.POST:
            suite_id = request.POST["suite_id"]
        if "name" in request.POST:
            name = request.POST["name"]
        if "project_name" in request.POST:
            project_name = request.POST["project_name"]    
        if "test_type" in request.POST:
            test_type = request.POST["test_type"]    
        if "test_case_list" in request.POST:
            test_case_list = request.POST["test_case_list"]
        if "created_by" in request.POST:
            created_by = request.POST["created_by"]
        if suite_id:
            this_suite = get_obj_else_None(TestSuite, pk=suite_id)
            if this_suite:
                this_suite.name = name
                this_suite.project_name = project_name
                this_suite.test_type = test_type
                this_suite.test_case_list = test_case_list                
                this_suite.created_by = created_by            
                this_suite.save()
            else:
                raise(Exception("Suite not found id=" + str(suite_id)))
        else:
            new_obj = TestSuite(name = name, project_name = project_name, 
            test_type = test_type, test_case_list = test_case_list, created_by = created_by)
            new_obj.save()  
        
    except Exception as err:
        logger.error(traceback.print_exc())        
        context['error'] = str(err)
      
    return testSuite(request, context)
    
def editSuiteHelp(request, context = {}, this_suite = None):
    if not request:
        raise(Exception("request is empty."))
        
    for key, value in request.POST.items():
        logger.info(key + "=" + str(value))
    t_types = []
    for key, value in TEST_TYPE_CHOICES:
        t_types.append(key)
    context['testtypes'] = t_types
    
    
    pro_0 = ""
    if "pro_0" in request.POST:
        pro_0 = request.POST["pro_0"]
    temp_name = ""
    if "temp_name" in request.POST:
        temp_name = request.POST["temp_name"]
    temp_crt_by = ""
    if "temp_crt_by" in request.POST:
        temp_crt_by = request.POST["temp_crt_by"] 
    temp_ttype_0 = ""
    if "temp_ttype_0" in request.POST:
        temp_ttype_0 = request.POST["temp_ttype_0"]
        
    if not temp_name:
        if this_suite:
            temp_name = this_suite.name
        else:    
            temp_name = ""
    context['temp_name'] = temp_name
    
    if not temp_crt_by:
        if this_suite:
            temp_crt_by = this_suite.created_by
        else:    
            temp_crt_by = ""
    context['temp_crt_by'] = temp_crt_by
    
    if not temp_ttype_0:
        if this_suite:
            temp_ttype_0 = this_suite.test_type
        else:    
            temp_ttype_0 = t_types[0]
    context['tname_0'] = temp_ttype_0
    
    if this_suite:
        context['suite_id'] = this_suite.id
    
    pro_list = []
    proj_obs = Projects.objects.all()            
    for item in proj_obs:
        pro_list.append(item.name)        
    context['projs'] = pro_list
    if not pro_0:
        if this_suite:
            pro_0 = this_suite.project_name
        else:
            pro_0 = pro_list[0]    
    context['pro_0'] = pro_0
    candidates = get_objs_else_None(TestCase, project_name=pro_0)
    #logger.info("proj:" + pro_0)
    #logger.info("candidates:" + str(candidates))
    if not candidates:
        for proj in pro_list:
            candidates = get_objs_else_None(TestCase, project_name=proj)
            logger.info("proj:" + proj)
            logger.info("candidates:" + str(candidates))
            if candidates:
                pro_0 = proj
                context['pro_0'] = proj
                break
    context['c_candidates'] = candidates
        
    if this_suite:
        context['test_case_list'] = this_suite.test_case_list
        case_list = this_suite.test_case_list.split(",")
        context['my_selected_case'] = case_list        
        group_list = ""
        for case_item in case_list:
            if "$" in case_item:
                case_splits = case_item.split(r"$")
                if len(case_splits)==1:
                    continue
                elif len(case_splits)==2:
                    if group_list and case_item[0] not in group_list:
                        group_list = group_list + "," + case_item[0]
                    else:
                        group_list = case_item[0]
                else:
                    pass
        logger.info("group_list:" + str(group_list))
        context['group_list'] = group_list

    
def addSuite(request):
    context = {}
    try:
        editSuiteHelp(request, context, None) 
        #logger.info("context:" + str(context))
        return render(request, os.path.join(intTemplatePath, "testsuite_form.html"), context)
        
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
        context['error'] = err_msg
    #logger.info("context:" + str(context))
    return testSuite(request, context)
    
def editSuite(request, pk):  
    context = {}
    try:
        this_suite = get_obj_else_None(TestSuite, pk=pk)
        if not this_suite:
            raise(Exception("Suite not found with pk=" + str(pk)))
        editSuiteHelp(request, context, this_suite)
        return render(request, os.path.join(intTemplatePath, "testsuite_update_form.html"), context)
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
        context['error'] = err_msg
        
    return testSuite(request, context)    


def displayIns(request, **myKeyArgs):
    logger.info("myKeyArgs:type=" + str(type(myKeyArgs)) + "value=" + str(myKeyArgs))    
    testsuite = TestSuite.objects.all()
    data = {}
    from_web = False
    if not testsuite:
        logger.warning("No testsuite")
    else:
        try:            
            if myKeyArgs:
                if "command_list" in myKeyArgs and myKeyArgs["command_list"]:
                    command_list = myKeyArgs["command_list"]
                    logger.info("command_list:" + str(command_list))
                    data["isTrueRun"] = False
                    data["command_list"] = command_list
                if "error" in myKeyArgs and myKeyArgs["error"]:
                    data["error"] = myKeyArgs["error"]                    
                if "from_web" in myKeyArgs and myKeyArgs["from_web"]:
                    from_web = myKeyArgs["from_web"]    
            instances = get_objs_else_None(TestInstance, created_from_web=from_web)
            project_name = ""            
            inst_meta = {}
            logger.info("instances " + str(len(instances)))
            if instances:            
                for instance in instances:
                    try:
                        for key, value in instance.__dict__.items():
                            logger.info("key=" + str(key) + ";value=" + str(value))
                        test_case_str_list, testsuite_obj, error_msg = \
                            getTestcaseByInstance(instance)
                        
                        logger.info("instance name:" + instance.name + \
                        " instance.progress " + str(instance.progress))
                        project_name = testsuite_obj.project_name
                        this_res = my_folderHelper.get_resource(project_name)                    
                        this_res = os.path.join(this_res, testsuite_obj.test_type)
                        if not os.path.isdir(this_res):
                            os.mkdir(this_res)
                        inst_meta_list=[]
                        module_name_list = []
                        case_list = myTestcaseHelper.splitCases(test_case_str_list)
                        
                        logger.info("type case_list:" + str(type(case_list)))
                        case_index = 0
                        for case_obj in case_list:                        
                            if case_obj.module_name in module_name_list:
                                continue
                            module_name_list.append(case_obj.module_name)                            
                            #create ts_mux folder here
                            if len(module_name_list)>1:
                                this_res = os.path.join(this_res, str(case_obj.module_name))
                                if not os.path.isdir(this_res):
                                    os.mkdir(this_res)
                                this_res = os.path.join(this_res, "ts_mux")
                                if not os.path.isdir(this_res):
                                    os.mkdir(this_res)
                            
                            #get git info
                            module = case_obj.module_name
                            repos_root = my_folderHelper.get_reposit(project_name)
                            relative_path = my_defaults.getRelativePathByModule(project_name,\
                                module)
                            wkdir_git = os.path.join(repos_root, relative_path)
                            git_info = repositoryHelper(project_name, wkdir_git, logger)
                            
                            this_meta = {"project_name":project_name,
                                "module_name":case_obj.module_name,
                                "gitserver":git_info.serverName(),
                                "branch":git_info.currentBranchName()
                            }
                            
                            
                            #get test set info                        
                            ts_object = get_obj_else_None(ActiveTestSet, 
                                        instance_id=instance.id,
                                        case_index_in_instance=case_index
                                        )
                            
                            if ts_object:
                                if this_meta:
                                    this_meta.update({"TestSet":ts_object.name})
                                else:
                                    this_meta = {"TestSet":ts_object.name}
                            if this_meta:
                                inst_meta_list.append(this_meta)                        
                            case_index = case_index + 1
                        inst_meta[instance.id] = inst_meta_list
                        #delete invalid if any and display
                        case_num = len(case_list)                     
                        ts_objects = get_objs_else_None(ActiveTestSet, 
                                        instance_id=instance.id
                                        ) 
                        for obj in ts_objects:
                            logger.info("ts:" + obj.name)
                            if obj.case_index_in_instance >= case_num:
                                logger.info("delete ")
                                obj.delete()
                        ts_objects = get_objs_else_None(ActiveTestSet, 
                                        instance_id=instance.id
                                        )
                        for obj in ts_objects:
                            logger.info("ts:" + obj.name)
                        #delete end
                        
                    except Exception as err:
                        logger.error(traceback.print_exc())
                        data["error"] = str(err)
                        continue
            else:
                logger.warning("No instance")
            
            data["test_instance"] = instances
            data["inst_meta"] = inst_meta
            data["project_name"] = project_name                    
            
            for key in inst_meta.keys():
                logger.info("inst_meta:" + str(key) + str(inst_meta[key]) + "\n")
                
        except Exception as err:
            logger.error("Exception:" + str(err))
            logger.error(traceback.print_exc())
            data["error"] = str(err)
    htmlstr = ""
    logger.info("data:" + str(data) + "\n")
    if from_web:
        htmlstr = "home.html"
    else:
        htmlstr = "instance_CICD_pipe.html"
    return render(request, os.path.join(navigatorPath, htmlstr), {"data_inst": data})    
    

def index(request, **myKeyArgs ):
    myKeyArgs["from_web"] = True
    return displayIns(request, **myKeyArgs)

def instance_cicd(request, **myKeyArgs):
    myKeyArgs["from_web"] = False
    return displayIns(request, **myKeyArgs)
    
def indexAfterFake(request, command_list, created_from_web):
    if created_from_web:
        return index(request, **{"command_list": command_list})
    else:
        return instance_cicd(request, **{"command_list": command_list})
        
def scheduled_list(request):    
    job_list = myScheduler.get_jobs()
    schedules = []
    logger.info("scheduled_list:", str(job_list))
    for item in job_list:
        one_schedule = {}
        logger.info(str(item.id))
        splits = item.id.split("_")
        if splits and splits[1]:
            instance = get_obj_else_None(TestInstance, pk=splits[1])          
            if instance:
                one_schedule["id"]=item.id
                one_schedule["name"]=instance.name
                one_schedule["suite"]=instance.testsuite
                one_schedule["trigger"]=item.trigger
                one_schedule["next_run_time"]=item.next_run_time
                logger.info(instance.name)
                logger.info(str(instance.testsuite))
                logger.info(str(item.trigger))
                logger.info(str(item.next_run_time))
                schedules.append(one_schedule)
    
    return render(request, os.path.join(navigatorPath, "scheduled_list.html"), {"schedule_inst": schedules})

def editSchedule(request, schedule_id):
    return

def deleteSchedule(request, schedule_id):
    myScheduler.remove_job(schedule_id)
    return scheduled_list(request)

def displayLog(request, pk):
    rep_name = ""
    suite_name = ""
    instance_name = ""
    test_type = ""
    context = {}
    keyword = ""
    
    try:
        if "rep_name" in request.POST:
            rep_name = request.POST["rep_name"]
            logger.info("rep_name:" + rep_name)
        if "suite_name" in request.POST:
            suite_name = request.POST["suite_name"]
            logger.info("suite_name:" + suite_name)
        if "instance_name" in request.POST:
            instance_name = request.POST["instance_name"]
            logger.info("instance_name:" + instance_name)
        if "project_name" in request.POST:
            project_name = request.POST["project_name"]
            logger.info("project_name:" + project_name)
            
        log_root = my_folderHelper.get_log(project_name)    
        my_logger = logHelper(rep_name, suite_name, instance_name, log_root)        
        file_content=[]
        file_content.append("suite_name:"+suite_name+"\n")
        file_content.append("instance_name:"+instance_name+"\n\n")
        file_content.append("log_root:"+log_root+"\n\n")
        file_content.extend([my_logger.getLogCotent()])    
        my_analyzer.reset(file_content)
        
        context['file_content'] = file_content    
        context['ins_id'] = pk
        return render(request, os.path.join(intTemplatePath, "logDisplay.html"), context)
    
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)    
    return viewReport(request, pk)

def highlightLogKey(request):
    context = {}
    keyword = ""
    ins_id = None
    try:
        if "keyword" in request.POST:
            keyword = request.POST["keyword"]
        if "ins_id" in request.POST:
            ins_id = request.POST["ins_id"]
        keyword = keyword.strip()
        if not keyword:
            raise Exception("keyword is empty.")
        logger.info("highlightLogKey keyword:" + keyword)
        context['ins_id'] = ins_id        
        context['file_content'] = my_analyzer.highlightLogKey(keyword)
            
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)    
    return render(request, os.path.join(intTemplatePath, "logDisplay.html"), context)
    
def InsMonitor_fun(instance, isTrueRun):
    logger.info("thrdInsMonitor start")
    if isTrueRun:
        while instance.test_status != "Finished":
            instance.progress =  99 if (instance.progress + 1) >= 100 else (instance.progress + 1)
            instance.save()
            logger.info("thrdInsMonitor:"+instance.name+"is running at " + \
            (str(datetime.datetime.now(tz=timezone.utc))))        
            time.sleep(30)
    if instance.test_result == "Pass":
        instance.progress = 100
        instance.save()
    logger.info("thrdInsMonitor end")
    
def run_instance_thread_fun(instance_id, isTrueRun = True):    
    command_list=[]
    logger.info("isTrueRun:" + str(isTrueRun))
    
    try:            
        instance, test_case_str_list, testsuite_obj, error_msg = getTestcaseByInstanceID(instance_id)
        if not test_case_str_list or not instance or not testsuite_obj:
            raise(Exception("error: test_case_str_list:",str(test_case_str_list),\
            "\ninstance:",str(instance),"\ntestsuite_obj:",str(testsuite_obj)))
        #if isTrueRun:
        thrdInsMonitor = threading.Thread(target=InsMonitor_fun, args=(instance, isTrueRun,))
        logger.info("thrdInsMonitor:Start at " + str(datetime.datetime.now(tz=timezone.utc)))
        thrdInsMonitor.start()
        repos_root, script_root, log_root, report_root, \
            resource_root = my_folderHelper.get_all(testsuite_obj.project_name)  
        #logger.info("project_name:" + testsuite_obj.project_name)
        my_report = reportHelper(testsuite_obj.name, instance.name, report_root, logger) 
        my_log_file = logHelper(my_report.getReportName(), testsuite_obj.name, instance.name, log_root)
        logger.info("run_instance_thread_fun: \nrepos_root:" +\
            repos_root + "\nscript_root" + script_root + "\nlog_root" + \
            log_root + "\nreport_root" + report_root)
        my_log_file_path = my_log_file.getLogFilePath()
        #my_log_file_path = sys.stdout.getLogFilePath()
        logger.info("run_instance_thread_fun:Log file path:" + my_log_file_path)
        instance.test_result = "Is Running"
        instance.test_status = "Is Running"
        instance.progress = 1
        instance.last_run = datetime.datetime.now(tz=timezone.utc)
        instance.save()
        config_info = { "have_config_file_as_input":False, "file_path":""}                
        
        script_path = script_root
        case_chart = []        
        total_case_no = len(test_case_str_list)
        #get test suite structure by parsing test_case_str_list, save to case_chart
        case_chart = myTestcaseHelper.splitCases(test_case_str_list, 1)
        logger.info("run_instance_thread_fun case_chart:" + str(case_chart))        
        
        startTotalTime = datetime.datetime.now(tz=timezone.utc)
        case_index = 0
        #case_chart = [[{"para_casename_1": rep1}, {"para_casename_2": rep2}],...]
        group_index = 0
        for case_group in case_chart:
            count_per_group = len(case_group)
            index_in_group = 0
            group_index = group_index + 1 
            myRunHelper = runningHelper( my_report.getReportDir(), \
                testsuite_obj.project_name, my_defaults, logger )
            for case in case_group:
                case_name = ""
                rep = None
                for key, value in case.items():
                    case_name = key
                    rep = value
                logger.info("case_name:" + case_name + ";rep:" + str(rep))
                
                index_in_group = index_in_group + 1
                bIsPPC = False    
                logger.info("run_instance_thread_fun: name:" + case_name + ", rep=" + rep
                    + " round " + str(case_index) + " begin. \n")                
                
                case_i = get_obj_else_None(TestCase, name=case_name)
                if not case_i:
                    raise OSError("run_instance_thread_fun:testcase name:" + case_name
                        + " no longer exist.")
                        
                relative_path = my_defaults.getRelativePathByModule(testsuite_obj.project_name,\
                    case_i.module_name)
                if not relative_path:
                    continue
                wkdir_git = os.path.join(repos_root, relative_path)
                #wkdir_git = os.path.join(wkdir_git, case_i.module_name)                
                logger.info("wkdir_git:" + wkdir_git)
                git_info = repositoryHelper(testsuite_obj.project_name, wkdir_git, logger)
                command = ""
                if case_i.executable_type == "py_script":
                    command = r"python3"
                    if OS != "Windows":                    
                        append_command = r" |tee -a "  + my_log_file_path
                    else:
                        command = r"python"
                        append_command = r" >" + my_log_file_path + r"&& type " +\
                                        my_log_file_path
                elif case_i.executable_type == "shell":
                    command = "sh"
                    if OS != "Windows":                    
                        append_command = r" |tee -a "  + my_log_file_path
                    else:                        
                        append_command = r" >" + my_log_file_path + r"&& type " +\
                                        my_log_file_path
                else:
                    pass
                case_meta = {"branchName":git_info.currentBranchName(), 
                    "serverName":git_info.serverName(), "currentCommitID":git_info.currentCommitID()}
                my_report.newCaseMeta(case_i.name, case_meta)
                
                if not case_i.executable:
                    raise OSError("Error: case_i.executable empty:" + str(case_i.executable))
                logger.info("resource_root=" + resource_root)
                ts_dir = my_folderHelper.getTsDir(resource_root, \
                        testsuite_obj.test_type, case_i.module_name, instance_id, case_index)
                executable_full = os.path.join(os.path.dirname(repos_root), case_i.executable)
                runner_path = os.path.dirname(executable_full)
                os.chdir(runner_path)
                ts_object = get_obj_else_None(ActiveTestSet, instance_id=instance.id,
                    case_index_in_instance=case_index )
                para_in = case_i.parameter_list                
                para_str = command_full = ""
                if case_i.executable_type == "shell" or case_i.executable_type == "py_script":
                    if case_i.executable_type == "shell":
                        if not para_in:
                            command_full = command + " " + executable_full + "" + append_command
                            command_list.append(command + " " + executable_full)
                    else:
                        py_file = executable_full
                        logger.info("py_file" + py_file)
                        if testsuite_obj.project_name.startswith("ROMA"):
                            if case_i.module_name=="ppc": 
                                logger.info("ppc")
                                command_full = command + " " + py_file + " " + para_in
                                command_list.append(command_full)
                            else:
                                processed_para = para_ins = []
                                if para_in:
                                    para_ins = para_in.split()
                                    logger.info("para_in:" + para_in)
                                    
                                processed_para = myRunHelper.verifyPara(ts_dir, ts_object, \
                                    case_i.module_name, para_ins)
                                para_str = ""
                                if processed_para:                           
                                    para_str = " ".join(processed_para)
                                    logger.info("\npara_str=" + para_str + "\n")
                                    logger.info("case_i.module_name=" + case_i.module_name + "\n")
                                    myRunHelper.preProcess(processed_para, ts_dir, case_i.module_name)                                    
                                    
                                #command_full = command + " \"" + py_file + "\" " + 
                                #    para_str + append_command  
                                command_full = command + " " + py_file + " " + para_str + \
                                    append_command
                                command_list.append(command + " " + py_file + " " + para_str)
                                
                                #config_info["have_config_file_as_input"] = True
                                #config_info["file_path"] = os.path.join(ts_dir + para_ins[0])                               
                                        
                        my_report.addCaseMeta({"exe":py_file, "para":str(para_str)})                
                    my_report.addCaseMeta({"exe_abr":case_i.executable, 
                        "para_abr":case_i.parameter_list})                
                    logger.info("command_full:\n" + command_full + "\n")
                    logger.info("isTrueRun:" + str(isTrueRun) + "\n")
                    if not isTrueRun:
                        continue                
                    for iter in range(int(rep)):
                        logger.info("run_instance_thread_fun:run case for the " + str(iter + 1)
                            + "th time\n")
                        ret_val = 0
                        startTime = datetime.datetime.now(tz=timezone.utc)
                        old_stdout = sys.stdout
                        sys.stdout = my_log_file
                        ret_val = os.system(command_full)
                        sys.stdout.saveLogToReport()
                        sys.stdout = old_stdout
                        #command_sequence = shlex.split(command_full)                                       
                        #p = subprocess.Popen(command_full, shell=True, cwd=cwd_tmp, 
                            #stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                        #while p.poll() is None:
                            #stdoutline = p.stdout.readline().decode('utf-8')                        
                       
                        endTime = datetime.datetime.now(tz=timezone.utc)
                        if ret_val != 0:
                            instance.test_result = "Failed"
                            logger.info("run_instance_thread_fun:case " + case_name + 
                                " round " + str(iter + 1) + " failed. \n")                                
                            my_report.addCaseResult( str(iter + 1), "Failed", 
                            "return value:"+str(ret_val), startTime, endTime)
                        logger.info("\n")
                        logger.info("ret_val:" + str(ret_val))
                        if ret_val == 0:
                            logger.info("post processing for " + case_i.module_name)
                            myRunHelper.postProcess(ts_dir, case_i.module_name)                            
                        
                        endTime = datetime.datetime.now(tz=timezone.utc)                    
                        if ret_val == 0:
                            my_report.addCaseResult( str(iter + 1), "Pass", "No error.", 
                                startTime, endTime)
                        else:
                            instance.test_result = "Failed"
                            logger.info("run_instance_thread_fun:case " + 
                                case_name + " round " + str(iter + 1) + " failed. \n")                                
                            my_report.addCaseResult( str(iter + 1), "Failed",
                                "return :"+str(ret_val), startTime, endTime)
                        
                        logger.info("run_instance_thread_fun:The " + str(iter + 1) + 
                            "th round of running case(" + case_name + ") completed.\n")
                
                else:
                    py_file = os.path.join(script_path, str(case_i.executable))                            
                    #command = command + " "  + py_file
                    logger.info("run_instance_thread_fun:py_file:" + py_file + "\n")
                    para = str(case_i.parameter_list)
                    for iter in range(rep):
                        logger.info("run_instance_thread_fun:run case for the " + 
                            str(iter + 1) + "th time\n")
                        startTime = datetime.datetime.now(tz=timezone.utc)
                        p = subprocess.Popen([command, py_file, para], 
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                        while p.poll() is None:
                            stdoutline = str(p.stdout.readline())                                
                            if "b'" in stdoutline:
                                stdoutline = stdoutline[stdoutline.index("b'")+2:]
                                if "'" in stdoutline:
                                    stdoutline = stdoutline[:stdoutline.index("'")]                                    
                            logger.info(stdoutline)                                
                        stdout, stderr = p.communicate()
                        endTime = datetime.datetime.now(tz=timezone.utc)
                        my_report.addCaseResult( str(iter + 1), "Pass", "No error.", 
                            startTime, endTime)
                        logger.info("run_instance_thread_fun:The " + str(iter + 1) + 
                            "th round of running case(" + case_name + ") completed.\n")
                
                        #if stdout:
                            #logger.info("stdout:" + str(stdout))                                                                
                
                
                logger.info("run_instance_thread_fun:The " + str(case_index) + 
                    "th case(" + case_name + ") finished. \n")
                case_index = case_index + 1
        endTotalTime = datetime.datetime.now(tz=timezone.utc)
        logger.info("run_instance_thread_fun:test_suite " + testsuite_obj.name + " finished. \n")
        my_report.addGlobals({"startTotal":str(startTotalTime), 
        "endTotal":str(endTotalTime),
        "durTotal":str(endTotalTime - startTotalTime)})
        if not isTrueRun:
            my_report.addGlobals({"runType": "Fake Run"})
        else:
            my_report.addGlobals({"runType": "True Run"})
        
        my_report.setTrueRun(isTrueRun)
        my_report.writeReport(testsuite_obj.project_name, repos_root, config_info)                    
        if isTrueRun:
            sendmail(my_report.getReportName())
    
        instance.test_result = "Pass" 
        instance.progress = 100
    except Exception as err:
        logger.info("run_instance_thread_fun:Error found!")
        instance.test_result = "Failed"
        instance.test_status = "Finished"
        traceback.print_exc()
        run_instance_is_running_tag = False
    finally:        
        instance.test_status = "Finished"
        instance.command_list = " ".join(command_list)
        instance.save()        
        obj_run_tbl = get_obj_else_None(RunningTable, instance_id=instance_id, 
            is_true_run = isTrueRun)
        if obj_run_tbl:        
            obj_run_tbl.fake_run_result = instance.command_list            
            obj_run_tbl.is_running = False
            logger.info("obj_run_tbl.fake_run_result=" + obj_run_tbl.fake_run_result + "\n" )
            obj_run_tbl.save()
        else:
            logger.info("run_instance_thread_fun Error:" + 
                "The instance can't be found in RunningTable.")
    

def run_instance_thread(instance_id, isTrueRun = True):
    try:
        instance, test_case_str_list, testsuite_obj, error_msg = getTestcaseByInstanceID(instance_id)
        logger.info("testsuite_obj.project_name:" + testsuite_obj.project_name)
        project_to_run = testsuite_obj.project_name
        #When you want a true run, while there's another true run of the SAME project 
        #isn't finished yet, just return
        if isTrueRun:
            running_ins = get_objs_else_None(RunningTable, project_name=project_to_run, 
            is_running = True, is_true_run = True)
            if running_ins:                 
                raise OSError("Warning: One" + project_to_run + " test is running.")
                
        #Fake run normally finishes in seconds. If not, you should check the network.
        #If an unfinished Fake-run exists in table, Fake run will be ignored. Just return.
        running_ins = get_obj_else_None(RunningTable, instance_id=instance_id, is_running = True,
            is_true_run = isTrueRun)
        if running_ins:
            #fake run should be finished in seconds. If not need kill.
            raise OSError("Warning: The instance is still running.isTrueRun="+str(isTrueRun))
            
        #If an unfinished True run exists in table, Fake run is permitted;
        #If an unfinished Fake run exists in table, True run is permitted, this rarely happens.
        logger.info("instance_id:" + str(instance_id) + "\nisTrueRun:" + str(isTrueRun))
        #check instance_id + is_true_run record existence:
        inses_to_run = get_objs_else_None(RunningTable, instance_id=instance_id, 
            is_true_run = isTrueRun)
        new_record = None
        if not inses_to_run:
            new_record = RunningTable(instance_id=instance_id, project_name=project_to_run, 
            is_running = True, 
            start_datetime = datetime.datetime.now(tz=timezone.utc), is_true_run = isTrueRun)
        else:
            logger.info("inses_to_run length:" + str(len(inses_to_run)))
            logger.info("Deleting duplicated ones")
            #Sometimes there may have garbage records. Delete when len>1 to recover the table.
            if len(inses_to_run) > 1:
                index_i = 0
                for inst in inses_to_run:
                    index_i = index_i + 1
                    if index_i < len(inses_to_run):
                        inst.delete()
                    else:
                        break
                inses_to_run = get_objs_else_None(RunningTable, instance_id=instance_id, 
                    is_true_run = isTrueRun)
                logger.warning("After deleting, inses_to_run length should be 1:" + \
                    str(len(inses_to_run)))
                new_record = inses_to_run[0]
            elif len(inses_to_run) == 1:
                new_record = inses_to_run[0]
            else:
                return
        
        new_record.project_name=project_to_run
        new_record.is_running = True
        new_record.start_datetime = datetime.datetime.now(tz=timezone.utc)
        new_record.save()
        logger.info("run_instance_thread:Creating thread at " + \
            str(datetime.datetime.now(tz=timezone.utc)))
        thread = threading.Thread(target=run_instance_thread_fun, args=(instance_id, isTrueRun,))
        logger.info("run_instance_thread:Starting thread at " + \
            str(datetime.datetime.now(tz=timezone.utc)))
        thread.start()
        if not isTrueRun:    
            thread.join()
            logger.info("run_instance_thread:End of thread.")
            ins = get_obj_else_None(RunningTable, instance_id=instance_id, is_true_run = isTrueRun)
            ins.is_running = False
            ins.save()
              
    except:
        logger.info("run_instance_thread: Error: unable to run instance.")
        logger.error(traceback.print_exc())
    
    
def runInstanceCICD(request):
    logger.info("This is invoked from runInstanceCICD.\n")
    for key in request.POST.items():
        logger.info("POST:", key, "=", str(request.POST[key]))
        
    instance = get_obj_else_None(TestInstance, created_from_web=False)
    return runInstance(request, instance.id, True)   
    data = {"result":1}
    return JsonResponse(data)
    

def runInstance(request, instance_id, isTrueRun = True):
    try:
        run_instance_thread(instance_id, isTrueRun)
        if not isTrueRun: 
            ins_run = get_obj_else_None(RunningTable, instance_id=instance_id, is_true_run = isTrueRun)
            
            instance = get_obj_else_None(TestInstance, pk=instance_id) 
            if ins_run and instance:
                if instance.created_from_web:
                    logger.info("runInstance fake_run_result:" + str(ins_run.fake_run_result))
                    return index(request, **{"command_list":ins_run.fake_run_result})
                else:
                    return instance_cicd(request, **{"command_list":ins_run.fake_run_result}) 
    except:
        logger.error( str(traceback.print_exc()) )
    return HttpResponseRedirect(reverse('integratedTest:index'))



def fakeRunInstance(request, instance_id):
    return runInstance(request, instance_id, False)

def refreshBranchInfo(request, pk):    
    test_case_str_list, instance, testsuite_obj, err_msg = None, None, None, ""
    try:
        instance, test_case_str_list, testsuite_obj, err_msg = getTestcaseByInstanceID(pk)
        logger.info("instance:" + str(instance))
        logger.info("test_case_str_list:" + str(test_case_str_list))
        logger.info("testsuite_obj:" + str(testsuite_obj))
        logger.info("err_msg:" + err_msg)
        project_name = testsuite_obj.project_name            
        repos_root = my_folderHelper.get_reposit(project_name)
        logger.info("repos_root:" + repos_root)            
        repository_info_list=[]
        module_name_list = []
        case_list = myTestcaseHelper.splitCases(test_case_str_list)
        for case_obj in case_list:
            if case_obj.module_name in module_name_list:
                continue
            module_name_list.append(case_obj.module_name)                
            repoinfo = get_obj_else_None(Repository, project_name=project_name, \
                    module_name=case_obj.module_name)
                    
            if not repoinfo:
                err_msg = "Repository obj not exist: project_name:" + project_name\
                + "\tmodule_name:" + case_obj.module_name
                raise err_msg    
            
            relative_path = my_defaults.getRelativePathByModule(testsuite_obj.project_name,\
                    case_obj.module_name)
            if not relative_path:
                err_msg = "Default failed: project_name:" + project_name\
                    + "\tmodule_name:" + case_obj.module_name
                raise err_msg
            repos_path = os.path.join(repos_root, relative_path)            
            logger.info("repos path:" + repos_path)
            git_info = repositoryHelper(project_name, repos_path, logger)
            branch_msg = git_info.currentBranchName()
            gitserver_msg = git_info.serverName()
            logger.info("refreshBranchInfo branch_msg:" + branch_msg)
            logger.info("refreshBranchInfo gitserver_msg:" + gitserver_msg)                
            #command, return_code, errormsg = git_info.gitPull()
            for key, value in repoinfo.__dict__.items():
                logger.info(str(key) + "=" + str(value))
            if branch_msg:
                repoinfo.branch_msg = branch_msg
            if gitserver_msg:
                repoinfo.gitserver_msg = gitserver_msg
            #if revision_hash:
                #repoinfo.revision_hash = revision_hash
            logger.info("refreshBranchInfo repoinfo 2:")
            for key, value in repoinfo.__dict__.items():
                logger.info(str(key) + "=" + str(value))
            repoinfo.save()
        
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
    if instance and not instance.created_from_web:
        return instance_cicd(request, **{"error": err_msg}) 
    return index(request, **{"error": err_msg})    

def getConfigByCaseIndex(ins_id, caseidx=0):
    instance = get_obj_else_None(TestInstance, pk=ins_id)
    testsuite_obj = get_obj_else_None(TestSuite, name=instance.testsuite)
    if not testsuite_obj:
        raise(Exception("suite " + instance.testsuite + " doesn't exist."))
    
    project_name = testsuite_obj.project_name
    logger.info("project_name: " + project_name)
    resource_root = my_folderHelper.get_resource(testsuite_obj.project_name)
    logger.info("resource_root=" + resource_root)
    resource_root = os.path.join(resource_root, testsuite_obj.test_type)
    test_case_str_list = testsuite_obj.test_case_list.split(",")
    case_list = myTestcaseHelper.splitCases(test_case_str_list)
    logger.info("case_list: " + str(case_list))
    caseidx = 0            
    case_i = case_list[0]
    resource_path = os.path.join(resource_root, str(case_i.module_name) )
    
    ts_object = get_obj_else_None(ActiveTestSet, 
        instance_id = ins_id, case_index_in_instance = caseidx)                   
    if not ts_object:
        raise(Exception("TestSet doesn't exist or no config file needed for this instance."))
    logger.info("ActiveTestSet ts_object: " + ts_object.name)
    logger.info("ActiveTestSet instance_id: " + str(ins_id)) 
    logger.info("ActiveTestSet case_index_in_instance: " + str(caseidx))
    logger.info("ActiveTestSet config_fname: " + ts_object.config_fname)
    config_path = os.path.join(resource_path, ts_object.name )
    logger.info("config file dir: " + config_path)
    if not os.path.isdir(config_path):
        raise(Exception("Dir doesn't exist:" + config_path))
    file_list = []
    listOfFile = os.listdir(config_path)
    for file_name in listOfFile:
        if project_name.startswith("ROMA"):
            if case_i.executable_type=="py_script":
                if file_name.endswith(r".yaml") or file_name.endswith(r".yml"):
                    file_list.append(file_name)            
                    continue
    return file_list, instance.created_from_web, instance.name, config_path, \
        ts_object.name, ts_object.config_fname, project_name
    
def editConfig(request, pk):    
    err_msg = ""
    created_from_web = True
    case_index = 0
    try:
        #refresh instance.last_touched
        instance, test_case_str_list, testsuite_obj, error_msg = \
            getTestcaseByInstanceID(pk)
        file_list, created_from_web, ins_name, config_path, ts_name, \
            config_fname, project_name = getConfigByCaseIndex(pk, case_index)
        if len(file_list)==0:
            raise(Exception("There's no config file found or defined for this instance.\
            You may try fakerun or run directly.") )                   
        else:
            act_type = ""
            if "act_type" in request.POST:
                act_type = request.POST["act_type"]
            if act_type:
                logger.info("act_type=" + act_type)
            file_clk = ""
            if "file_clk" in request.POST:
                file_clk = request.POST["file_clk"]
            if file_clk:
                logger.info("file_clk=" + file_clk)
            
            if act_type=="open_another" and file_clk:
                file_to_open = file_clk
            else:
                if config_fname:
                    file_to_open = config_fname
                else:
                    file_to_open = file_list[0]
            file_content= ""
            with open(os.path.join(config_path, file_to_open), "r") as my_file:
                lines = my_file.readlines()
                file_content = "".join(lines)
            #logger.info("file_content:\n\'" + str(file_content) + "\'\n")    
            ascii_str = ""
            for char_r in file_content:
                if str(ord(char_r))=="10":
                    ascii_str = ascii_str + str(ord(char_r)) + "\n"
                else:
                    ascii_str = ascii_str + str(ord(char_r)) + " "
            #logger.info("file_content:\n\'" + ascii_str + "\'\n")
            
            frepo_clk = ""
            if "frepo_clk" in request.POST:
                frepo_clk = request.POST["frepo_clk"]
            if frepo_clk:
                logger.info("frepo_clk=" + frepo_clk)
            config_inrepo = []
            config_inrepo_f0 = ""
            f_content_repo= ""
            config_repo = my_defaults.getConfigPath()
            relative_path = ""
            if config_repo:
                path_splits = config_repo[project_name]
                if path_splits:
                    relative_path = os.path.sep.join(path_splits)
                    repo_root = my_folderHelper.get_reposit(project_name)
                    if repo_root:
                        repo_conf_path = os.path.join(repo_root, relative_path)
                        logger.info("repo_conf_path:" + repo_conf_path)                        
                        listOfFile = os.listdir(repo_conf_path)
                        for file_name in listOfFile:
                            if file_name.endswith(r".yaml") or file_name.endswith(r".yml"):
                                config_inrepo.append(file_name)
                                if not config_inrepo_f0:
                                    config_inrepo_f0 = file_name
                                if frepo_clk==file_name:
                                    config_inrepo_f0 = file_name
                                
                                with open(os.path.join(repo_conf_path, config_inrepo_f0), "r")\
                                    as my_file:
                                    lines = my_file.readlines()
                                    f_content_repo = "".join(lines)
                                                               
                        
            return render(request, os.path.join(intTemplatePath, "editConfig.html"), \
            {'file_content':file_content, 'file_list':file_list, \
            'file_0':file_to_open, 'ins_id':pk, 'ins_name':ins_name,\
            'folder_path':config_path, 'ts_name':ts_name, 'sep':os.path.sep,\
            'config_repo':config_inrepo, 'relative_path':relative_path,\
            'config_inrepo_f0':config_inrepo_f0,'f_content_repo':f_content_repo}) 
    
    except Exception as ex:
        logger.error(str(ex))
        logger.error(traceback.print_exc())
        err_msg = str(ex)
    
    if not created_from_web:
        return instance_cicd(request, **{"error": err_msg}) 
    return index(request, **{"error": err_msg})

def configSave(request, pk):
    err_msg = ""
    created_from_web = True
    case_index = 0
    try:        
        f_name = ""
        if "f_name" not in request.POST:
            raise(Exception("f_name doesn't existed in request."))
        f_name = request.POST["f_name"]
        if not f_name:
            raise(Exception("f_name is empty."))
        logger.info("f_name=" + f_name) 
        
        f_newname = ""
        if "new_name" in request.POST:
            f_newname = request.POST["new_name"]
        if f_newname:
            logger.info("f_newname=" + f_newname)
        
        folder_path = ""
        if "folder_path" not in request.POST:
            raise(Exception("folder_path doesn't existed in request."))
        folder_path = request.POST["folder_path"]
        if not folder_path:
            raise(Exception("folder_path is empty."))
        logger.info("folder_path=" + folder_path)
        
        save_as = None
        if "save_as" in request.POST:
            save_as = request.POST["save_as"]
        if save_as:
            logger.info("save_as=" + str(save_as) + ", type=" + str(type(save_as)))
        
        ch_active = None
        if "ch_active" in request.POST:
            ch_active = request.POST["ch_active"]
        if ch_active:
            logger.info("ch_active=" + str(ch_active) + ", type=" + str(type(ch_active)))
                
        f_content = ""
        if "f_content" not in request.POST:
            raise(Exception("f_content doesn't existed in request."))
        f_content = request.POST["f_content"] 
        if not f_content:
            raise(Exception("File content to save is empty."))
        f_content = str(bytes(f_content, 'utf-8').replace(b"\r\n", b"\n"), 'utf-8')
        f_name_to_save = f_name
        if save_as and f_newname:
            f_name_to_save = f_newname    
        full_path_conf = os.path.join(folder_path, f_name_to_save)
        with open(full_path_conf, "w") as outfile:
            outfile.writelines(f_content)
        if ch_active:
            ts_object = get_obj_else_None(ActiveTestSet, 
                        instance_id = pk, case_index_in_instance = case_index)
            if ts_object:
                logger.info("ts_object.name:" + ts_object.name)
                logger.info("ts_object.instance_id:" + str(ts_object.instance_id))
                logger.info("ts_object.config_fname before:" + ts_object.config_fname)
                ts_object.config_fname = f_name_to_save
                logger.info("ts_object.config_fname after:" + ts_object.config_fname)
                ts_object.full_path = full_path_conf
                ts_object.save()
                logger.info("ts_object.full_path:" + ts_object.full_path)
            else:
                raise(Exception("ts_object can't be found. Select testset first."))
                
        #for debug
        content_old = ""
        with open(os.path.join(folder_path, f_name), "r") as my_file:
            lines = my_file.readlines()
            content_old = "".join(lines)
            
        if content_old == f_content:
            logger.info("content_old == f_content")
        else:
            logger.info("content_old != f_content")
            left = ""
            for char in f_content:
                if str(ord(char))=="10":
                    left = left + str(ord(char)) + "\n"
                    #logger.info("str(ord(char)):\n\'" + str(ord(char)) + "\'\n")
                else:
                    left = left + str(ord(char)) + " "
            logger.info("f_content:\n\'" + left + "\'\n")
            
            right = ""
            for char_r in content_old:
                if str(ord(char_r))=="10":
                    right = right + str(ord(char_r)) + "\n"
                else:
                    right = right + str(ord(char_r)) + " "
            logger.info("content_old:\n\'" + right + "\'\n")
        #end for debug
        
        
    except Exception as ex:
        logger.error(traceback.print_exc())
        logger.error(traceback.print_exc())
        err_msg = str(ex)
    if not created_from_web:
        return instance_cicd(request, **{"error": err_msg}) 
    return index(request, **{"error": err_msg})
    
def viewConfig(request, pk):    
    err_msg = ""
    created_from_web = True
    
    try:
        #refresh instance.last_touched
        instance, test_case_str_list, testsuite_obj, error_msg = \
            getTestcaseByInstanceID(pk)
        file_list, created_from_web, ins_name, config_path, ts_name, config_fname\
           , project_name = getConfigByCaseIndex(pk, 0)
        if len(file_list)==0:
            raise(Exception("There's no config file found or defined for this instance.\
            You may try fakerun or run directly.") )        
        else:
            with open(os.path.join(config_path, file_list[0]), "r") as my_file:
                lines = my_file.readlines()
                file_content=[]
                file_content.append(file_list[0]+":\n")
                file_content.extend(lines)    
                return HttpResponse(file_content, content_type="text/plain")
    
    except Exception as ex:
        logger.error(str(ex))
        logger.error(traceback.print_exc())
        err_msg = str(ex)
    if not created_from_web:
        return instance_cicd(request, **{"error": err_msg}) 
    return index(request, **{"error": err_msg})

def checkPythonModule(request):
    p = subprocess.Popen(["pip", "list"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)    
    stdout, stderr = p.communicate() 
    return HttpResponse(stdout, content_type="text/plain")

def selBranch(request, pk):
    instance = None
    err_msg = ""
    try:
        #refresh instance.last_touched
        instance, test_case_str_list, testsuite_obj, err_msg = \
            getTestcaseByInstanceID(pk)
        
        clked_module_branch = ""
        branch_selected = ""
        module_current = ""            
        git_branch_all_info = {}
        old_branch = ""
        old_commitID = ""
        logger.info("request.POST.items():")
        for key, value in request.POST.items():
            logger.info("key=" + key)
            logger.info("value=" + value)
        if "module_branch" in request.POST:
            clked_module_branch = request.POST["module_branch"]
        logger.info("clked_module_branch=" + clked_module_branch)
        project_name = testsuite_obj.project_name
        logger.info("project_name: " + project_name)
        selected_repo_path = ""
        if clked_module_branch:
            clk_br_mod_splts = clked_module_branch.split(",")                
            if len(clk_br_mod_splts)== 2:
                module_current = clk_br_mod_splts[0]
                branch_selected = clk_br_mod_splts[1]
                selected_repo_path = my_defaults.getRelativePathByModule(project_name,\
                    module_current)
                logger.info("selected_repo_path=" + str(selected_repo_path))
                if not selected_repo_path in git_branch_all_info:
                    git_branch_all_info[selected_repo_path] = {
                        "branch":branch_selected,
                        "commit_list":[]
                    }
        br_candidatelist = {}            
        
        repos_root = my_folderHelper.get_reposit(project_name) 
        
        test_type = testsuite_obj.test_type
        module_name = ""            
        case_list = myTestcaseHelper.splitCases(test_case_str_list)
        
        for case_i in case_list:
            brCandidatesPerCase = {}
            
            module_name = case_i.module_name        
            relative_path = my_defaults.getRelativePathByModule(project_name,\
                module_name)
            if not relative_path:
                err_msg = "Default failed: project_name:" + project_name\
                    + "\tmodule_name:" + module_name
                raise err_msg
            wkdir_git = os.path.join(repos_root, relative_path)            
            logger.info("repos path:" + wkdir_git)
            git_info = repositoryHelper(project_name, wkdir_git, logger)
            #Note: Many modules share one repo; Many repo may serve one project
            this_branch = ""
            if relative_path in git_branch_all_info:
                if not "branch" in git_branch_all_info[relative_path] or \
                    not git_branch_all_info[relative_path]["branch"]:
                    this_branch = git_info.currentBranchName()
                    git_branch_all_info[relative_path]["branch"] = this_branch
                else:
                    this_branch = git_branch_all_info[relative_path]["branch"]
                if not "commit_list" in git_branch_all_info[relative_path] or\
                    not git_branch_all_info[relative_path]["commit_list"]:
                    git_branch_all_info[relative_path]["commit_list"] = \
                        git_info.getAvailableCommitList(this_branch)
            else:
                git_branch_all_info[relative_path] = {} 
                this_branch = git_info.currentBranchName()
                git_branch_all_info[relative_path] = {
                    "branch":this_branch,
                    "commit_list":git_info.getAvailableCommitList(this_branch)
                }
            
            if not old_commitID:
                old_commitID = git_info.currentCommitID()
            brCandidatesPerCase[module_name] = {
                "selected_br":this_branch,
                "localBrList":git_info.localBranches(),
                "remoteBrList":git_info.remoteBranches(),
                "local_des":git_info.currentBranchDescription(),
                "commitList":git_branch_all_info[relative_path]["commit_list"],                    
                "currentCommitID":old_commitID,
                "currentCommitDesc":git_info.getDescriptionByCommitID(old_commitID),
                }
            br_candidatelist[case_i.name] = brCandidatesPerCase
        #end for
        logger.info("br_candidatelist:" + str(br_candidatelist))
        return render(request, os.path.join(intTemplatePath, "selectBranch.html"), \
        {'br_list':br_candidatelist, 'project_name':project_name, \
          'inst_name':instance.name, 'suite_name':testsuite_obj.name,\
        'test_type':test_type, 'pk':pk,
        'created_from_web':instance.created_from_web})
        
    except Exception as ex:
        logger.error(traceback.print_exc())
        err_msg = str(ex)
    
    if instance and not instance.created_from_web:
        return instance_cicd(request, **{"error": err_msg}) 
    return index(request, **{"error": err_msg})
    
def submitBranchSelect(request):
    err_msg = ""
    try:
        if "project_name" in request.POST and "test_type" in request.POST\
            and "created_from_web" in request.POST:
            project_name = request.POST["project_name"]
            test_type = request.POST["test_type"]
            created_from_web = request.POST["created_from_web"]        
            
            logger.info("request.POST.items():")
            for key, value in request.POST.items():
                logger.info("key=" + key)
                logger.info("value=" + value)
            repos_root = my_folderHelper.get_reposit(project_name)
            all_modules = my_defaults.getRelativePathByPro(project_name)
            for this_module in all_modules:
                if this_module in request.POST:
                    this_branch = request.POST[this_module]
                    relative_path = my_defaults.getRelativePathByModule\
                        (project_name, this_module)
                    if not relative_path:
                        err_msg = "Non existing relative_path in default file for "\
                            + project_name + "\tmodule_name:" + this_module
                        raise err_msg
                    wkdir_git = os.path.join(repos_root, relative_path)            
                    logger.info("repos path:" + wkdir_git)
                    git_info = repositoryHelper(project_name, wkdir_git, logger)
                    cur_branch = git_info.currentBranchName()
                    this_commit = ""
                    if this_module + "_commit" in request.POST:
                        this_commit = request.POST[this_module + "_commit"]
                    use_commit = False
                    if this_module + "_check" in request.POST:
                        use_commit = True
                    cur_commitID = git_info.currentCommitID()
                    #detached
                    if not cur_branch:
                        if this_branch:
                            command, returncode, err_msg = \
                                git_info.changeBranch(this_branch)
                            if returncode:
                                err_msg = command + " failed:" + err_msg
                                raise(Exception(err_msg))
                            else:
                                err_msg = "Complete in success.Switch to "\
                                + git_info.currentBranchName()
                        else:
                            logger.info("this_branch is empty for" + this_module)
                            continue
                        cur_branch = this_branch
                    
                    if this_branch == cur_branch and this_commit and \
                        cur_commitID == this_commit:
                        logger.info("No need to to anything. No change.")
                        continue
                    else:
                        if this_branch != cur_branch:
                            command, returncode, err_msg = \
                                git_info.changeBranch(this_branch)
                            if returncode:
                                err_msg = command + " failed:" + err_msg
                                raise(Exception(err_msg))
                            else:
                                err_msg = "Complete in success.Switch to "\
                                + git_info.currentBranchName()
                                
                        if use_commit and this_commit and \
                            cur_commitID != this_commit:
                            if "(" in this_commit:
                                inx = this_commit.index("(")
                                this_commit = this_commit[:inx]
                            
                            command, returncode, err_msg = \
                                git_info.changeBranch(this_commit)
                            if returncode:
                                err_msg = command + " failed:" + err_msg
                                raise(Exception(err_msg))
                            else:
                                err_msg = "Complete in success.Switch to "\
                                + git_info.currentCommitID()
            
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
    logger.info("created_from_web:" + str(created_from_web))
    if created_from_web.strip()=="False":
        return instance_cicd(request, **{"error": err_msg})
    else:
        return index(request, **{"error": err_msg})
        
def selTestSet(request, pk):    
    err_msg = ""    
    try:
        #refresh instance.last_touched
        instance, test_case_str_list, testsuite_obj, err_msg = \
            getTestcaseByInstanceID(pk)        
        
        ts_candidatelist = []
        project_name = testsuite_obj.project_name
        logger.info("project_name: " + project_name)
        resource_root = my_folderHelper.get_resource(project_name)
        resource_root = os.path.join(resource_root, testsuite_obj.test_type) 
        test_type = testsuite_obj.test_type        
        module_name = ""
        case_index = 0
        case_list = myTestcaseHelper.splitCases(test_case_str_list)
        for case_i in case_list:                    
            module_name = case_i.module_name
            bFound = False
            ts_object = get_obj_else_None(ActiveTestSet, 
                instance_id = pk, case_index_in_instance = case_index)
            logger.info("ts_object:" + str(ts_object))
            if ts_object:
                logger.info("ts_object instance_id:" + str(ts_object.instance_id))
                logger.info("ts_object case_index_in_instance:" + str(ts_object.case_index_in_instance))
            tsCandidates = []                    
            resource_path = os.path.join(resource_root, str(module_name) )                    
            if os.path.isdir(resource_path):                        
                file_list = []
                listOfFile = os.listdir(resource_path)
                #Ensure we have dataset for test
                ts_selected = ""
                ts_selected_full = ""
                for fname in listOfFile:                            
                    ts_dir_full = os.path.join(resource_path, fname)                            
                    ts_dir_name = fname
                    if os.path.isdir(ts_dir_full) and ts_dir_name.startswith("ts_"):
                        #logger.info("selTestSet.debug 5")
                        tobeShowFirst = False
                        if not ts_selected:
                            ts_selected = ts_dir_name
                            ts_selected_full = ts_dir_full
                        if ts_object and (ts_object.name == ts_dir_name):                                    
                            bFound = True 
                            tobeShowFirst = True
                            if ts_object.full_path != ts_dir_full:
                                logger.info("Warning:" + \
                                ts_object.full_path + "!=" + ts_dir_full)                                        
                                ts_object.full_path = ts_dir_full
                                logger.info("ts_object.full_path updated.")
                                ts_object.save()                                    
                            
                        if tobeShowFirst:
                            tsCandidates.append([ts_dir_name, ts_dir_full, "selected"])
                        else:
                            tsCandidates.append([ts_dir_name, ts_dir_full, "unselected"])
                        #logger.info("tsCandidates append:" + str(tsCandidates))
                logger.info("bFound:" + str(bFound))
                if not bFound:
                    #update if not exist, we need one
                    if ts_selected and ts_selected_full:
                        if ts_object:
                            ts_object.name = ts_selected
                            ts_object.full_path = ts_selected_full
                            ts_object.save()
                        else:
                            new_obj = ActiveTestSet(
                                instance_id = pk, 
                                case_index_in_instance = case_index,
                                name = ts_selected,
                                full_path = ts_selected_full,
                                )
                            new_obj.save()
                    else:
                        #data set not exist any more, the folder is empty. Should delete.
                        logger.info("No testset exists. Need create a new one or run previous step once")
                        if ts_object:
                            ts_object.delete()
            else:
                #if resource_path not exist, stay at Home page
                err_msg = "resource_path not exist:", resource_path
                raise(Exception(err_msg))
            
            ts_candidatelist.append({
                "case_name": case_i.name,
                "case_index":case_index, 
                "module":module_name,
                "candidates":tsCandidates
            })
            case_index = case_index + 1
        #end for
        #logger.info("ts_candidatelist:" + str(ts_candidatelist))
        return render(request, os.path.join(intTemplatePath, "selectTestSet.html"), \
        {'ts_list':ts_candidatelist, 'project_name':project_name, \
        'ins_id':pk,
        'case_list':test_case_str_list, 'err_msg':err_msg, \
        'created_from_web':instance.created_from_web})
       
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
  
    if instance and not instance.created_from_web:
        return instance_cicd(request, **{"error": err_msg}) 
    return index(request, **{"error": err_msg})

def SubmitTestSet(request):
    err_msg = ""
    try:
        if "bHaveChanged" in request.POST and "tsStringChanged" in request.POST\
            and "ins_id" in request.POST:        
            created_from_web = request.POST["created_from_web"]        
            bHaveChanged = request.POST["bHaveChanged"]
            tsStringChanged = request.POST["tsStringChanged"]
            ins_id = request.POST["ins_id"]
            
            if bHaveChanged == "T":
                ts_items = tsStringChanged.split(";")
                #logger.info("ts_items:" + str(ts_items))
                for ts_item in ts_items:
                    if not ts_item:
                        break
                    else:
                        ts_parts = ts_item.split(",")
                        logger.info("ts_parts:" + str(ts_parts))
                        if len(ts_parts) != 3:
                            logger.info("SubmitTestSet: ts_parts something missing.")
                            continue
                        case_index = ts_parts[0]
                        ts_object = get_obj_else_None(ActiveTestSet, 
                            instance_id = ins_id,
                            case_index_in_instance = case_index)
                        
                        if ts_object:
                            ts_object.full_path = ts_parts[2]
                            ts_object.name = ts_parts[1]
                            ts_object.save()    
                        else:
                            new_obj = ActiveTestSet(
                                instance_id = ins_id,
                                case_index_in_instance = case_index,
                                name = ts_parts[1],
                                full_path = ts_parts[2])
                            new_obj.save()
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
        
    logger.info("created_from_web:" + str(created_from_web)) 
    if created_from_web.strip()=="False":
        return instance_cicd(request, **{"error": err_msg}) 
    return index(request, **{"error": err_msg})


    
def settings(request):
    context = {}
    action = ""
    logger.info("request.POST.items:")
    for key, value in request.POST.items():
        logger.info(key + "=" + str(value))
    if "action" in request.POST:
        action = request.POST["action"].strip()
    if action=="start_add":
        return render(request, os.path.join(intTemplatePath, 'addEmail.html'))
        
    del_email = ""
    if action=="del" and "email" in request.POST:
        del_email = request.POST["email"].strip()
    
    rec_name = ""
    if action=="add" and "rec_name" in request.POST:
        rec_name = request.POST["rec_name"].strip()
    rec_email = ""
    if action=="add" and "rec_email" in request.POST:
        rec_email = request.POST["rec_email"].strip()
    
    logger.info("action:" + action + " del_email:" + del_email + \
    " rec_name:" + rec_name + " rec_email:" + rec_email)    
    emails = []
    email_file = os.path.join(os.path.dirname(__file__),\
        'report_receiver_list.txt')
    with open(email_file, mode='r', encoding='utf-8') as contacts_file:
        for a_contact in contacts_file:
            if del_email and del_email.strip()==a_contact.strip():
                continue
            emails.append(a_contact)
    if action=="add" and rec_name and rec_email:
        emails.append(rec_name + " " + rec_email + "\n")
        
    if action=="del" or action=="add":
        with open(email_file, mode='w', encoding='utf-8') as contacts_file:
            for line in emails:
                contacts_file.write(line)
    logger.info("emails:") 
    logger.info(str(emails))    
    context["emails"] = emails
    
    return render(request, os.path.join(navigatorPath, 'settings.html'), context)

def sysclone(request):
    context = {}
    return render(request, os.path.join(navigatorPath, 'sysclone.html'), context)

def syscl_submit(request):
    context = {}
    for key, value in request.POST.items():
        logger.info("key=" + key)
        logger.info("value=" + value)
    #dir = os.path.cwd()
    #fname = os.path.join(dir, "sysclone.json")
    fname = "sysclone.json"
    
    if "export" in request.POST and request.POST["export"] == "on":
        logger.info("export")
        if os.path.isfile(fname):
            bakupname = "bakup_on_" + str(datetime.datetime.now())\
            .replace(".", "_").replace(" ", "_").replace(":", "_") + "_" + fname
            os.rename(fname, bakupname) 
            logger.info("File backuped:" + bakupname)
            
        with open( fname, "w") as fhandler:
            logger.info("import")
            all_contents = {"settings":{}, "cases":[], 
            "suites":[], "stances":[] }
            
                    
            if "exportcase" in request.POST and \
                request.POST["exportcase"] == "on":
                logger.info("save case.")
                case_objs = get_objs_else_None(TestCase)
                for one_case in case_objs:
                    if one_case.name.startswith("__"):
                        continue
                    all_contents["cases"].append({
                        "name":one_case.name,
                        "executable":one_case.executable.split(os.path.sep),
                        "parameter_list":one_case.parameter_list,
                        "executable_type":one_case.executable_type,
                        "project_name":one_case.project_name,
                        "module_name":one_case.module_name,
                        "created_by":one_case.created_by
                    })
                   
                            
            if "exportsuite" in request.POST and \
                request.POST["exportsuite"] == "on":
                logger.info("save suite.")
                suite_objs = get_objs_else_None(TestSuite)
                for one_suite in suite_objs:
                    if one_suite.name.startswith("__"):
                        continue
                    all_contents["suites"].append({
                        "name":one_suite.name,
                        "test_type":one_suite.test_type,
                        "test_case_list":one_suite.test_case_list,                        
                        "project_name":one_suite.project_name,                        
                        "created_by":one_suite.created_by
                    })                    
                            
            if "exportinstance" in request.POST and \
                request.POST["exportinstance"] == "on":
                logger.info("save instance.")                
                instance_objs = get_objs_else_None(TestInstance)
                for one_instance in instance_objs:
                    if one_instance.name.startswith("__"):
                        continue
                    all_contents["stances"].append({
                        "name":one_instance.name,
                        "testsuite":one_instance.testsuite,                                              
                        "created_by":one_instance.created_by
                    }) 
                    
            json.dump(all_contents, fhandler)
        context["msg"] = "Exporting succeeded on " + str(datetime.datetime.now(tz=timezone.utc))
    else :
        if not os.path.isfile(fname):
            context["msg"] = "File can't be found:" + fname
        else:
            with open( fname ) as fhandler:
                logger.info("import")
                all_defaults = json.load(fhandler)
                logger.info("json.load end")
                cases = all_defaults["cases"]
                logger.info(str(cases))
                suites = all_defaults["suites"]
                logger.info(str(suites))
                instances = all_defaults["stances"]
                logger.info(str(instances))
                if "importcase" in request.POST and \
                    request.POST["importcase"] == "on":
                    logger.info("load case.")
                    for one_case in cases:
                        if one_case["name"].startswith("__"):
                            continue
                        case_obj = get_obj_else_None(TestCase, name = one_case["name"])
                        if not case_obj:
                            new_obj = TestCase(name = one_case["name"], 
                            executable = os.path.sep.join(one_case["executable"]),
                            parameter_list = one_case["parameter_list"], 
                            executable_type = one_case["executable_type"], 
                            project_name = one_case["project_name"], 
                            module_name = one_case["module_name"],
                            created_by = one_case["created_by"], 
                            create_datetime = timezone.now)
                            new_obj.save()
                        continue
                        
                if "importsuite" in request.POST and \
                    request.POST["importsuite"] == "on":
                    logger.info("load suite.")
                    for one_suite in suites:
                        if one_suite["name"].startswith("__"):
                            continue
                        suite_obj = get_obj_else_None(TestSuite, name = one_suite["name"])
                        if not suite_obj:            
                            new_obj = TestSuite(name = one_suite["name"], 
                            test_type = one_suite["test_type"],
                            test_case_list = one_suite["test_case_list"], 
                            project_name = one_suite["project_name"],
                            created_by = one_suite["created_by"],
                            create_datetime = timezone.now)
                            new_obj.save()                        
                        continue
                        
                if "importinstance" in request.POST and \
                    request.POST["importinstance"] == "on":
                    logger.info("load instance.")
                    for one_instance in instances:  
                        if one_instance["name"].startswith("__"):
                            continue
                        instance_obj = get_obj_else_None(TestInstance, \
                            name = one_instance["name"])
                        if not instance_obj:            
                            new_obj = TestInstance(name = one_instance["name"], 
                            testsuite = one_instance["testsuite"],                
                            created_by = one_instance["created_by"],
                            create_datetime = timezone.now)
                            new_obj.save()
                            new_obj = get_obj_else_None(TestInstance, \
                                name = one_instance["name"])
                            logger.info("new_obj.id=", str(new_obj.id))                        
                            new_ts_obj = ActiveTestSet(
                                name = "ts_default", 
                                instance_id = new_obj.id )
                            new_ts_obj.save()
            
            context["msg"] = "Importing succeeded on " + str(datetime.datetime.now(tz=timezone.utc))

    return render(request, os.path.join(navigatorPath, 'sysclone.html'), context)


    
def listGlobalSetting(request):
    my_globalSettingNameList = []
    my_globalSettings = []
    context = {}
    hostname = platform.node()
    logger.info(hostname)
    my_globalSettings = GlobalSetting.objects.filter(hostname=hostname)
    my_set = GlobalSetting.objects.all()
    for item in my_set:
        for key, value in item.__dict__.items():
            if key == "hostname":
                logger.info(str(key)+"=" + str(value))
    if my_globalSettings:
        my_globalSettings = my_globalSettings.order_by("name").order_by("-is_active")
        logger.info("my_globalSettings:" + str(my_globalSettings))
        context['setting_selected'] = my_globalSettings[0]
        logger.info(str(context['setting_selected']))
        if "setting_select" in request.POST:
            setting_selected_by_name = request.POST["setting_select"]
            for current_set in my_globalSettings:
                if current_set.name == setting_selected_by_name:
                    context['setting_selected'] = current_set
                else:
                    pass
                my_globalSettingNameList.append(current_set.name)
        else:
            for current_set in my_globalSettings:
                my_globalSettingNameList.append(current_set.name)
        # for key, value in context['setting_selected'].__dict__.items():
        # logger.info(str(key)+"=" + str(value))
    context['hostname'] = hostname
    context['os'] = OS
    context['name_list'] = my_globalSettingNameList
    return render(request, os.path.join(navigatorPath, 'globalsetting.html'), context)

class AddGlobalSettingView(CreateView):
    model = GlobalSetting
    form_class = GlobalSetUpdateForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        hostname = platform.node()
        context['hostname'] = hostname
        logger.info("hostname AddGlobalSettingView:" + str(context['hostname']))
        context['os'] = OS
        logger.info("os AddGlobalSettingView:" + str(context['os']))
        pro_list = []
        proj_obs = Projects.objects.all()            
        for item in proj_obs:
            pro_list.append(item.name)
        context['projnames'] = pro_list
        context['name_0'] = pro_list[0]
        return context

    def get_success_url(self):
        return reverse("integratedTest:globalsetting")

def testsetSubmit(request):
    tsname = ""
    project_name = ""
    test_type = ""
    module_name = ""
    sel_cwd = ""    
    error = ""

        
    if "tsname" in request.POST:
        tsname = request.POST["tsname"].strip()
        
    if "project_name" in request.POST:
        project_name = request.POST["project_name"].strip()
        
    if "test_type" in request.POST:
        test_type = request.POST["test_type"].strip()
        
    if "module_name" in request.POST:
        module_name = request.POST["module_name"].strip()
    if "cwd" in request.POST:
        sel_cwd = request.POST["cwd"].strip()
    dir_selected = ""    
    if "dirItem" in request.POST:
        dir_selected = request.POST["dirItem"].strip()
    file_selected = ""
    if "fileItem" in request.POST:
        file_selected = request.POST["fileItem"].strip()
    wavdir = ""
    if "wavdir" in request.POST:
        wavdir = request.POST["wavdir"].strip()
    logger.info("sel_cwd" + sel_cwd) 
    logger.info("dir_selected" + dir_selected)
    logger.info("file_selected" + file_selected)
    logger.info("wavdir" + wavdir)
    if not tsname or not tsname.startswith("ts_"):
        error = "Invalid test set name"
    else:
        resource_project = my_folderHelper.get_resource(project_name)
        logger.info("resource_project" + resource_project)
        resource_ttype = os.path.join(resource_project, test_type)
        logger.info("resource_ttype" + resource_ttype)
        if not os.path.isdir(resource_ttype):
            os.mkdir(resource_ttype)
        parent_path = os.path.join(resource_ttype, module_name )
        logger.info("parent_path" + parent_path)
        if not os.path.isdir(parent_path):
            os.mkdir(parent_path)
        if not os.path.isdir(parent_path):
            error = "Path not exist for " + project_name + test_type + module_name
        newts_path = os.path.join(parent_path, tsname)
        if not os.path.isdir(newts_path):
            os.mkdir(newts_path)
        
        src_yaml = os.path.join(sel_cwd, file_selected)
        dest_dir = os.path.join(sel_cwd, dir_selected)
        shutil.copy(src_yaml, newts_path)
        
        p = subprocess.Popen(["ln", "-sf", dest_dir, wavdir], cwd=newts_path, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        command = "ln -sf " + dest_dir + " " + wavdir
        logger.info(command)
        logger.info("stdout:" + out.decode('utf-8'))
        logger.info("stderr:" + error.decode('utf-8'))
        logger.info("return code:" + str(p.returncode))
            
    return testset(request, **{"error": error})
    
def testset(request, **myKeyArg):
    context = {}
    name_clk = ""
    clk_cwd = ""
    pro_list = []
    t_types = []
    module_names = []
    proj_obs = Projects.objects.all()            
    for item in proj_obs:
        pro_list.append(item.name)
    
    context['PROJS'] = pro_list    
    for key, value in TEST_TYPE_CHOICES:
        t_types.append(key)
    context['TTYPES'] = t_types
    for key, value in MODULE_CHOICES:
        module_names.append(key)
    context['MODULES'] = module_names    
    if "cwd" in request.POST:
        clk_cwd = request.POST["cwd"].strip()
    if "name_clk" in request.POST:
        name_clk = request.POST["name_clk"].strip()
    context['fileClked'] = "ts_"
    if "fileClked" in request.POST:
        fileClked = request.POST["fileClked"].strip()
        context['fileClked'] = fileClked
    context['curtsname'] = "ts_"
    if "curtsname" in request.POST:
        curtsname = request.POST["curtsname"].strip()
        context['curtsname'] = curtsname
    context['curproj'] = "ROMA"
    if "curproj" in request.POST:
        curproj = request.POST["curproj"].strip()
        context['curproj'] = curproj
    context['curttype'] = "integrated_test"
    if "curttype" in request.POST:
        curttype = request.POST["curttype"].strip()
        context['curttype'] = curttype
    context['cur_mod']="h5_composer"
    if "cur_mod" in request.POST:
        cur_mod = request.POST["cur_mod"].strip()
        context['cur_mod'] = cur_mod        
    if "error" in request.POST:
        error = request.POST["error"].strip()
        context['error'] = error
    if "error" in myKeyArg:
        context['error'] = myKeyArg['error']
    dataroot = ""
    if not name_clk or not clk_cwd:
        if OS == "Windows":
            dataroot = r"d:\data"
        else:
            dataroot = r"/"
    else:
        if name_clk == r"..":
            dataroot = os.path.dirname(clk_cwd)
        else:
            dataroot = os.path.join(clk_cwd, name_clk)
    
    logger.info("dataroot:" + dataroot)        
    listOfFile = os.listdir(dataroot)
    dirlist = []
    filelist = []    
    if os.path.basename(os.path.normpath(dataroot)) != "data":
        dirlist.append(r"..")
    for fname in listOfFile:
        myFile = os.path.join(dataroot, fname)
        if os.path.isfile(myFile):
            if fname.endswith(r".yml") or fname.endswith(r".yaml"):
                filelist.append(fname)
            else:
                continue
        else:                
            dirlist.append(fname)
            
    #resource_root = my_folderHelper.get_resource(testsuite_obj.project_name)
    #resource_root = os.path.join(resource_root, testsuite_obj.test_type)
    #resource_path = os.path.join(resource_root, str(case_i.module_name) )
    context['cwd'] = dataroot
    context['files'] = filelist
    context['dirs'] = dirlist
    return render(request, os.path.join(navigatorPath, 'testset.html'), context)
    
def reportBug(request):
    context = {}
    return render(request, os.path.join(navigatorPath, "reportbug.html"), context)


def sandboxView(request):
    context = {}
    return render(request, os.path.join(navigatorPath, "sandbox.html"), context)

def devicesView(request):
    context = {"device_list":["Simulator 1"]}
    return render(request, os.path.join(navigatorPath, "testdevice.html"), context)
    
def helpView(request):
    context = {}
    return render(request, os.path.join(navigatorPath, "help.html"), context)


def viewReport(request, pk):
    instance = get_obj_else_None(TestInstance, pk=pk)
    err_msg = ""
    if instance:
        try:            
            testsuite_objs = TestSuite.objects.filter(name=instance.testsuite)
            report_requested = ""
            if testsuite_objs:
                testsuite_obj = testsuite_objs[0]
                test_case_str_list = testsuite_obj.test_case_list.split(",")
                logger.info("test_case_str_list:" + str(test_case_str_list))            
                if "report_list_select" in request.POST:
                    report_requested = request.POST["report_list_select"]
                    logger.info("report_requested:" + str(report_requested))
                else:
                    logger.info("no request.report_list_select.")
                    for key, vlaue in request.POST.items():
                        pass
                        #logger.info("key:", key, " = ", vlaue)
                
                report_root = my_folderHelper.get_report(testsuite_obj.project_name)
                my_report = reportHelper(testsuite_obj.name, instance.name, \
                    report_root, logger, False)
                if report_requested:
                    report_data = my_report.readReport(report_requested)
                else:
                    report_data = my_report.readReport()
                
                context = {'filenamelist': report_data["filenamelist"],
                           'fname_to_display': report_data["fname_to_display"],
                           'rep_requested': report_data["rep_requested"],
                           'instance_name': instance.name,
                           'instance_id': pk,
                           'from_web':instance.created_from_web,
                           'suite_name': testsuite_obj.name,
                           'project_name': testsuite_obj.project_name,
                           'report_dir':my_report.getInsReportRoot()
                           }
                if "err" in request.POST:
                    context['err'] = request.POST["err"]
                logger.info("viewReport context:" + str(context))
                return render(request, os.path.join(intTemplatePath, "viewreport_form.html"), context)
        except Exception as err:
            logger.error(traceback.print_exc())
            err_msg = str(err)
    
    if instance and not instance.created_from_web:
        return instance_cicd(request, **{"error": err_msg}) 
    return index(request, **{"error": err_msg})


def reportCompare(request, pk):    
    err_msg = ""
    compare_with = ""
    compare_by = ""    
    project_name = ""
    inst_name = ""
    suite_name = ""
    left_name = ""
    srcfile = ""
    destfile = ""

    context = {}
    try:    
        if "compare_with" in request.POST:
            compare_with = request.POST["compare_with"]
        if "compare_by" in request.POST:
            compare_by = request.POST["compare_by"]
        if "project_name" in request.POST:
            project_name = request.POST["project_name"]
        if "inst_name" in request.POST:
            inst_name = request.POST["inst_name"]
        if "suite_name" in request.POST:
            suite_name = request.POST["suite_name"]    
        if "left_name" in request.POST:
            left_name = request.POST["left_name"]
        logger.info("reportCompare compare_with:" + compare_with)
        logger.info("reportCompare compare_by:" + compare_by)
        logger.info("reportCompare project_name:" + project_name)
        
        report_root = my_folderHelper.get_report(project_name)
        my_report = reportHelper(suite_name, inst_name, report_root, logger, False)
        if not my_report:
            raise(Exception("reportCompare reportHelper init failed."))
        insReportRoot = my_report.getInsReportRoot()    
        srcDir = os.path.join(insReportRoot, left_name)
        logger.info("reportCompare srcDir:" + srcDir)
        listOfFile = os.listdir(srcDir)
        for fname in listOfFile:
            if "compare" in compare_by and "Log" in compare_by:
                if not fname.endswith(r".log"):
                    continue
            if "compare" in compare_by and ("Yaml" in compare_by \
                or "Yml" in compare_by):
                if not fname.endswith(r".yaml") or not fname.endswith(r".yml"):
                    continue
            srcfile = os.path.join(srcDir, fname)   
        if not srcfile:
            raise(Exception("No log file found in " + srcDir))
        
        dir_dest = os.path.join(insReportRoot, compare_with)
        logger.info("reportCompare dir_dest:" + dir_dest)
        listOfFile = os.listdir(dir_dest)
        for fname in listOfFile:
            if "compare" in compare_by and ("Yaml" in compare_by \
                or "Yml" in compare_by):
                if not fname.endswith(r".yaml") or not fname.endswith(r".yml"):
                    continue
            if "compare" in compare_by and "Log" in compare_by:
                if not fname.endswith(r".log"):
                    continue
            destfile = os.path.join(dir_dest, fname)
        
        my_analyzer.reset()
        context['fileleft'], context['fileright'] = \
            my_analyzer.fileCompare(srcfile, destfile)        
        context['ins_id'] = pk
        return render(request, os.path.join(intTemplatePath, "compareReport.html"), context)
    
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
    
    if not request.POST._mutable:
        request.POST._mutable = True
    request.POST['err'] = err_msg
    return viewReport(request, pk)

def reportDisplayKey(request, pk):
    file_content_left=[]
    file_content_right=[]
    context = {}
    keyword = ""
    
    try:
        if "keyword" in request.POST:
            keyword = request.POST["keyword"]
        keyword = keyword.strip()
        if not keyword:
            raise Exception("keyword is empty.")
        logger.info("keyword:" + keyword)
            
        context['fileleft'], context['fileright'] = \
            my_analyzer.searchKey(keyword)    
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
    
    context['ins_id'] = pk
    return render(request, os.path.join(intTemplatePath, "compareReport.html"), context) 

def viewFile(request):
    fpath = ""
    file_content=[]
    try:
        if "fpath" in request.POST:
            fpath = request.POST["compare_with"]
        if not os.path.isfile(fpath):
            raise(Exception("viewFile File not exist:"+fpath))
        with open(fpath, "r") as my_file:
            lines = my_file.readlines()
            file_content.append(fpath+":\n")
            file_content.extend(lines)    
                                    
    except Exception as err:
        logger.error(traceback.print_exc())
        err_msg = str(err)
        file_content.append(err_msg+":\n")
        
    return HttpResponse(file_content, content_type="text/plain")
    
    
def viewReportDetail(request, pk):
    instance = get_obj_else_None(TestInstance, pk=pk)
    err_msg = ""
    report_dir = ""
    report_root = ""
    nameclked = ""
    clk_type = ""
    context = {}
    if instance:
        try:    
            if "report_root" in request.POST:
                report_root = request.POST["report_root"]
            if "nameclked" in request.POST:
                nameclked = request.POST["nameclked"]
            if "clk_type" in request.POST:
                clk_type = request.POST["clk_type"]
            context['dirs'], context['files'] = \
                my_fldr_brwsr.browse("viewReportDetail", nameclked, report_root)
            context['ins_id'] = pk
            logger.info("context:" + str(context))
            return render(request, os.path.join(intTemplatePath, "report_detail.html"), context)
        except Exception as err:
            logger.error(traceback.print_exc())
            err_msg = str(err)
    
    if not request.POST._mutable:
        request.POST._mutable = True
    request.POST['err'] = err_msg

    return viewReport(request, pk)
    
def scheduleInstanceView(request, pk):
    instance = get_obj_else_None(TestInstance, pk=pk)
    if instance:
        testsuite_obj = instance.testsuite        
        if testsuite_obj:
            scheduled_mode = "date"
            if "schedule_select" in request.POST:
                scheduled_mode = request.POST["schedule_select"]
            context = {
                'instance_id': pk,
                'instance_name': instance.name,
                'scheduled_mode': scheduled_mode
            }
            # logger.info("context:" + str(context))
            return render(request, os.path.join(intTemplatePath, 'instance_scheduler.html'), context)

    return HttpResponseRedirect(reverse('integratedTest:index'))


def afterScheduleSubmited(request, pk):
    instance = get_obj_else_None(TestInstance, pk=pk)
    if instance:
        testsuite_obj = instance.testsuite
        
        if testsuite_obj:        
            scheduled_mode = ""
            if "sche_mode" in request.POST:
                scheduled_mode = request.POST["sche_mode"]
            logger.info("scheduled_mode:" + scheduled_mode)
            if scheduled_mode == "date":
                if "run_time" not in request.POST:
                    logger.info("no request.run_time.")
                    for key, vlaue in request.POST.items():
                        logger.info("key:", key, "=", vlaue)
                else:
                    time_scheduled = request.POST["run_time"].replace("T", " ")+":00"
                    logger.info("\ntime scheduled at:" + time_scheduled)
                    myScheduler.add_job(runInstance, "date", 
                        [None, pk],
                        id="runInstance_"+str(pk),  
                        run_date=time_scheduled, replace_existing=True)
            elif scheduled_mode == "cron" or scheduled_mode == "interval":
                if "run_hour" not in request.POST or "run_minute" not in request.POST:
                    logger.info("request has no run_hour or run_minute when mode is " + scheduled_mode)
                else:
                    my_hour = request.POST["run_hour"]
                    if not my_hour:
                        my_hour = 0
                    my_minute = request.POST["run_minute"]
                    if not my_minute:
                        my_minute = 0
                    logger.info("\nmy_hour:" + my_hour)
                    logger.info("\nmy_minute:" + my_minute )
                    #trigger = cron.CronTrigger(hour=my_hour, minute=my_minute)
                    if scheduled_mode == "cron":
                        myScheduler.add_job(runInstance, scheduled_mode, 
                            [None, pk], timezone=get_localzone(), hour=my_hour, minute=my_minute,
                            id="runInstance_"+str(pk), replace_existing=True) 
                    else:
                        myScheduler.add_job(runInstance, scheduled_mode, 
                            [None, pk], timezone=get_localzone(), hours=int(my_hour), minutes=int(my_minute),
                            id="runInstance_"+str(pk), replace_existing=True)
            else:
                logger.info("\nNot support yet.")
            logger.info("\ntime scheduled end.")
   
    return HttpResponseRedirect(reverse('integratedTest:index'))
    
def createJiraTicket(request):
    context = {"data": {}}
    return render(request, os.path.join(intTemplatePath, "createJiraTicket.html"), context)

def jiraSubmit(request):
    for key in request.POST.items():
        logger.info("jiraSubmit:" + key)
    
    if "server" not in request.POST or "username" not in request.POST or  "token" not in request.POST or "summary" not in request.POST or "description" not in request.POST:
        logger.info("jiraSubmit failed.") 
    else: 
        server_str = request.POST["server"]
        username_str = request.POST["username"]
        token_str = request.POST["token"]        
            
        my_jira = JiraHandler(server_str, username_str, token_str, logger) 
        logger.info("my_jira created:" + str(my_jira))
        jira_ID = my_jira.create_jira_cr_ticket(request.POST["summary"], request.POST["description"])
        logger.info("jira_ID:" + str(jira_ID))
        context = {"data": {
                "summary":request.POST["summary"],
                "description":request.POST["description"],
                "jira_ID":jira_ID
            }
        }
    return render(request, os.path.join(intTemplatePath, "createJiraTicket.html"), context)   
    

class EditProjectView(UpdateView):
    model = Projects    
    template_name_suffix = '_update_form'
    form_class = ProjectsForm
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get(self.pk_url_kwarg)
        queryset = self.get_queryset().filter(pk=pk)
        proj = queryset.get()
        context['proj'] = proj
        context['pk'] = pk
        return context
        
    def get_success_url(self):
        return reverse("integratedTest:projects")
        
class EditGlobalSettingView(UpdateView):
    model = GlobalSetting
    form_class = GlobalSetUpdateForm
    template_name_suffix = '_update_form'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        pk = self.kwargs.get(self.pk_url_kwarg)
        queryset = self.get_queryset().filter(pk=pk)
        g_setting = queryset.get()
        hostname = platform.node()
        context['hostname'] = hostname
        context['os'] = OS
        context['my_setting'] = g_setting
        
        pro_list = []
        proj_obs = Projects.objects.all()            
        for item in proj_obs:
            pro_list.append(item.name)
        context['projnames'] = pro_list
                
        # for key, value in context['my_setting'].__dict__.items():
        # logger.info(str(key)+"=" + str(value))
        return context

    def get_success_url(self):
        return reverse("integratedTest:globalsetting")


class TestcaseDeleteView(DeleteView):
    model = TestCase
    template_name_suffix = '_confirm_delete'
    success_url = reverse_lazy('integratedTest:testCase')


class TestSuiteDeleteView(DeleteView):
    model = TestSuite
    template_name_suffix = '_confirm_delete'
    success_url = reverse_lazy('integratedTest:testSuite')

class InstanceDeleteView(DeleteView):
    model = TestInstance
    template_name_suffix = '_confirm_delete'
    success_url = reverse_lazy('integratedTest:index')

class InstDelCICDView(DeleteView):
    model = TestInstance
    template_name_suffix = '_confirm_delete'
    success_url = reverse_lazy('integratedTest:instance_cicd')

class DeleteGlobalSetting(DeleteView):
    model = GlobalSetting
    template_name_suffix = '_confirm_delete'
    success_url = reverse_lazy('integratedTest:globalsetting')

class DeleteProjectView(DeleteView):
    model = Projects
    template_name_suffix = '_confirm_delete'
    success_url = reverse_lazy('integratedTest:projects')
    
def cloneCase(request, pk):
    record = TestCase.objects.get(id=pk)
    name_str = record.name
    new_name_str = name_str + r"_" + time.ctime().replace(" ", "_").replace(":", "-")

    new_record = TestCase(name=new_name_str, executable=record.executable, \
    parameter_list=record.parameter_list, project_name=record.project_name,
    module_name=record.module_name, executable_type=record.executable_type, 
                          created_by=record.created_by, create_datetime=timezone.now())
    new_record.save()    
    return HttpResponseRedirect(reverse('integratedTest:testCase'))

def cloneInstance(request, pk):
    old_ins = TestInstance.objects.get(id=pk)

    name_str = old_ins.name
    new_name_str = name_str + r"_" + time.ctime().replace(" ", "_").replace(":", "-")

    new_record = TestInstance(name=new_name_str, testsuite=old_ins.testsuite, \
    test_status="Not Run", test_result="N/A", command_list = old_ins.command_list, \
    created_from_web = old_ins.created_from_web,\
    created_by=old_ins.created_by, create_datetime=timezone.now())
    new_record.save()
    if not old_ins.created_from_web:
        return HttpResponseRedirect(reverse('integratedTest:instance_cicd'))
    return HttpResponseRedirect(reverse('integratedTest:index'))

def cloneSuite(request, pk):
    record = TestSuite.objects.get(id=pk)

    name_str = record.name
    new_name_str = name_str + r"_" + time.ctime().replace(" ", "_").replace(":", "-")

    new_record = TestSuite(name=new_name_str, test_case_list=record.test_case_list, \
    project_name=record.project_name, test_type=record.test_type, created_by=record.created_by,
                           create_datetime=timezone.now())
    new_record.save()   
    return HttpResponseRedirect(reverse('integratedTest:testSuite'))

def cloneGlobalSetting(request, pk):
    record = GlobalSetting.objects.get(id=pk)

    name_str = record.name
    new_name_str = name_str + r"_" + time.ctime()

    new_record = GlobalSetting(name=new_name_str, hostname=record.hostname,
                               ostype=record.ostype, is_active=False,
                               user=record.user,
                               project_name=record.project_name)
    new_record.save()
    return listGlobalSetting(request)

def cloneProject(request, pk):
    record = get_obj_else_None(Projects, id=pk)
    name_str = record.name
    new_name_str = name_str + r"_" + time.ctime()

    new_record = Projects(name=new_name_str, module_list=record.module_list,
                               created_by=record.created_by)
    new_record.save()
    return HttpResponseRedirect(reverse('integratedTest:projects'))
    
    
def uploadScriptViewf(request):
    # Load documents for the list page
    context = {}
    documents = Document.objects.all()
    filename = ""
    if "file_path" in request.POST:
        filename = request.POST["file_path"]
    else:
        return HttpResponseRedirect(reverse('integratedTest:upload'))
    pro_list = []
    proj_obs = Projects.objects.all()            
    for item in proj_obs:
        pro_list.append(item.name)
    
    for item in documents:            
        if not os.path.exists(item.docfile.path):
            logger.info("File no longer exist and will be removed from the database:")
            logger.info(item.docfile.path)
            Document.objects.filter(docfile=item.docfile).delete()
    
    if not os.path.isfile(filename):
        logger.info("The log file not exist:" + filename) 
    file_content = ""
    try:
        with open(filename, "r") as outfile:
            file_content = outfile.readlines()
    except OSError:
        logger.error(traceback.print_exc())   
            
    form = DocumentForm() # A empty, unbound form        
    documents = Document.objects.all()    
    # Render list page with the documents and the form
    return render(request, os.path.join(navigatorPath, "upload.html"), context={'documents': documents, 'form': form, 'projnames': pro_list, 'name_0': pro_list[0], 'file_content': file_content})
    
def uploadScript(request):
    # Handle file upload
    context = {}
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            if "project_name" in request.POST:
                project_name = request.POST["project_name"]
            newdoc = Document(docfile = request.FILES['docfile'], project_name=project_name)            
            newdoc.save()
            logger.info("newdoc.save:"+str(newdoc.docfile.path))            
            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse('integratedTest:upload'))
    else:
        form = DocumentForm() # A empty, unbound form

    # Load documents for the list page
    documents = Document.objects.all()
    
    pro_list = []
    proj_obs = Projects.objects.all()            
    for item in proj_obs:
        pro_list.append(item.name)
    
    for item in documents:            
        if not os.path.exists(item.docfile.path):
            logger.info("File no longer exist and will be removed from the database:")
            logger.info(item.docfile.path)
            Document.objects.filter(docfile=item.docfile).delete()
        
    documents = Document.objects.all()    
    # Render list page with the documents and the form
    return render(request, os.path.join(navigatorPath, "upload.html"), context={'documents': documents, 'form': form, 'projnames': pro_list, 'name_0': pro_list[0]})
    

def testView(request):
    my_Repository = Repository.objects.all()
    for item in my_Repository:
        for key, value in item.__dict__.items():
            logger.info(str(key) + "=" + str(value))
    context = {'display_list': my_Repository}
    return render(request, os.path.join(navigatorPath, "test.html"), context)    


