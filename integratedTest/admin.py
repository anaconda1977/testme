from django.contrib import admin
from .models import TestCase

class TestCaseAdmin(admin.ModelAdmin):
    fields = ['name', 'executable', 'parameter_value_list', 'executable_type', 'test_type']

admin.site.register(TestCase, TestCaseAdmin)
