import os
import subprocess
import platform
import re

#Note: repository content may change frequently. 
#So don't use repositoryHelper as global variable. 
#Instead, use git_info = repositoryHelper() locally, 
#in order to update the related information in time. 
#Information here means those things stored in __init__ variables
#such as self.tagList etc.

class repositoryHelper():    
    def __init__(self, project_name, working_directory, logger=None):
        if not logger:
            return None
        self.logger = logger
        self.logger.info("project_name" + project_name)
        self.logger.info("repositoryHelper: working_directory" + working_directory)
        if not project_name or not working_directory or not os.path.isdir(working_directory):
            raise ValueError("no project name or working directory wrong")
        
        self.currentCheckoutIsDetached = False
        self.localBrnchList = []
        self.tagList = {}
        self.latestCommitIDList = {}
        self.commitIDbyTag = {}
        self.curBrnchNm = ""
        self.curCommitTag = ""
        
        self.project_name = project_name
        self.wdir = working_directory
        self.logger.info("working_directory:" + working_directory)        
    
    def currentBranchName(self):        
        p = subprocess.Popen(["git", "branch"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.logger.info("git branch")        
        out, error = p.communicate()                
        self.curBrnchNm = ""
        self.localBrnchList = []
        out_lines = out.decode('utf-8').split("\n")
        self.logger.info("out_lines")
        self.logger.info(out_lines)
        self.logger.info("out_lines length")
        self.logger.info(len(out_lines))
        for line in out_lines:
            thisBrnch = line.strip()
            if not thisBrnch:
                continue
            if r"* " in thisBrnch:
                index = thisBrnch.index(r"* ")                    
                thisBrnch = thisBrnch[index+2:]
                if thisBrnch.startswith("(HEAD detached at "):
                    self.currentCheckoutIsDetached = True
                    self.logger.info("currentCheckoutIsDetached=" + \
                        str(self.currentCheckoutIsDetached))
                    self.curBrnchNm = ""
                else:
                    self.localBrnchList.append(thisBrnch)
                    self.curBrnchNm = thisBrnch
                self.logger.info("branch-name-in-use=" + thisBrnch)
                self.logger.info("stderr:" + error.decode('utf-8'))
                self.logger.info("return code:" + str(p.returncode))                
            else:  
                self.localBrnchList.append(thisBrnch)
                
        return self.curBrnchNm      
        
    def serverName(self):  
        p = subprocess.Popen(["git", "remote", "-v"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.logger.info("git remote -v")
        
        gitserver_msg = ""
        while p.poll() is None:
            stdoutline = p.stdout.readline().decode("utf-8")
            if "https://" in stdoutline:                        
                index_h = stdoutline.index("https://")
                gitserver_msg = stdoutline[index_h:index_h + 8]
                
                if "@" in stdoutline:
                    index_at = stdoutline.index("@")
                    gitserver_msg = gitserver_msg + stdoutline[index_at + 1:]
                    
                else:
                    gitserver_msg = stdoutline[index_h + 8:] 
                                               
                if " " in gitserver_msg:
                    msglist = gitserver_msg.split()
                    gitserver_msg = msglist[0]
                    
        self.logger.info("gitserver-name-in-use=" + gitserver_msg)
              
        return gitserver_msg
    
    #As I tried, git rev-parse HEAD returns current CommitID, not HEAD CommitID    
    def currentCommitID(self):
        p = subprocess.Popen(["git", "rev-parse", "HEAD"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        self.logger.info("git rev-parse HEAD")
        self.logger.info("stdout:" + out.decode('utf-8'))
        self.logger.info("stderr:" + error.decode('utf-8'))
        self.logger.info("return code:" + str(p.returncode))  
        headCommitID = out.decode('utf-8').strip()    
        return headCommitID
    
    #current means current checkout. Not the latest commit.
    def gitLogCurrent(self):
        p = subprocess.Popen(["git", "log", "-1"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.logger.info("git log -s -5")
        des_msg = ""
        while p.poll() is None:
            line = p.stdout.readline().decode("utf-8").strip()
            self.logger.info(line)
            if not line:
                continue
            des_msg = des_msg + line
        '''
        out, error = p.communicate()
        des_msg = out.decode('utf-8').strip()        
        self.logger.info("stdout:" + out.decode('utf-8'))
        self.logger.info("stderr:" + error.decode('utf-8'))
        self.logger.info("return code:" + str(p.returncode))
        '''
        self.logger.info("des_msg=" + des_msg)
        return des_msg
        
    def currentBranchDescription(self):
        p = subprocess.Popen(["git", "branch", "-vv"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        descrbtns = out.decode('utf-8').strip().split("\n")        
        for descb in descrbtns:
            descb = descb.strip()
            if descb and descb.startswith("*"):
                self.logger.info("git branch -vv")
                self.logger.info("stdout:" + out.decode('utf-8'))
                self.logger.info("descb:" + descb)
                self.logger.info("stderr:" + error.decode('utf-8'))
                self.logger.info("return code:" + str(p.returncode))     
                return descb
        return ""
    
    def localBranches(self):
        #refresh local branches
        self.currentBranchName()
        self.logger.info("localBrnchList len=" + str(len(self.localBrnchList)))
        return self.localBrnchList
        
    #get remote branches    
    def remoteBranches(self):
        p = subprocess.Popen(["git", "fetch"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        self.logger.info("git fetch")          
        
        p = subprocess.Popen(["git", "branch", "-r"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        self.logger.info("git branch -r")
        out_lines = out.decode('utf-8').split("\n")
        self.logger.info("out_lines")
        self.logger.info(out_lines)
        branch_list = []
        for line in out_lines:
            thisBrnch = line.strip()
            if not thisBrnch:
                continue
            branch_list.append(thisBrnch) 
             
        self.logger.info("branch_list:") 
        self.logger.info(branch_list)    
        self.logger.info("remote branch_list len=" + str(len(branch_list)))
        self.logger.info("stdout:" + out.decode('utf-8'))
        self.logger.info("stderr:" + error.decode('utf-8'))
        self.logger.info("return code:" + str(p.returncode)) 
        return branch_list 
    
    def gitPull(self):
        p = subprocess.Popen(["git", "pull"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        self.logger.info("git pull")
        self.logger.info("stdout:" + out.decode('utf-8'))
        self.logger.info("stderr:" + error.decode('utf-8'))
        self.logger.info("return code:" + str(p.returncode))
        return "git pull", p.returncode, error.decode('utf-8')
        
    def changeBranch(self, newBranch):
        p = subprocess.Popen(["git", "stash"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        self.logger.info("git stash:")
        self.logger.info("stdout:" + out.decode('utf-8'))
        self.logger.info("stderr:" + error.decode('utf-8'))
        self.logger.info("return code:" + str(p.returncode))
        #if p.returncode:
            #return "git stash", p.returncode, error.decode('utf-8')
        
        #skip remotes/origin/ for remote
        if r"/" in newBranch:
            loc = newBranch.rindex(r"/")
            newBranch = newBranch[loc+1:]
        self.logger.info("Target branch name:" + newBranch)
        p = subprocess.Popen(["git", "checkout", newBranch], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #while p.poll() is None:
            #self.logger.info("In running...") 
        out, error = p.communicate()
        self.logger.info("git checkout " + newBranch)
        self.logger.info("stdout:" + out.decode('utf-8'))
        self.logger.info("stderr:" + error.decode('utf-8'))
        self.logger.info("return code:" + str(p.returncode))
        if p.returncode:
            return "git checkout " + newBranch, p.returncode, error.decode('utf-8')
        
        if "detached HEAD" in error.decode('utf-8'):
            return "git checkout " + newBranch, p.returncode, error.decode('utf-8')
            
        p = subprocess.Popen(["git", "pull"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        self.logger.info("git pull:")
        self.logger.info("stdout:" + out.decode('utf-8'))
        self.logger.info("stderr:" + error.decode('utf-8'))
        self.logger.info("return code:" + str(p.returncode))
        return "git checkout + pull", p.returncode, error.decode('utf-8')
    
    def getTagList(self):
        if self.tagList:
            self.logger.info("tagList:")
            self.logger.info(self.tagList)
            return self.tagList
        p = subprocess.Popen(["git", "tag"], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        self.tagList = out.decode('utf-8').split("\n")
        for item in self.tagList:
            if not item:
                self.tagList.remove(item)
        
                
        self.logger.info("git tag")
        self.logger.info("tagList:" + str(self.tagList))
        self.logger.info("stderr:" + error.decode('utf-8'))
        self.logger.info("return code:" + str(p.returncode))
        
        return self.tagList
        
        
    def tagCurrent(self):
        des_line = self.gitLogCurrent().split("\n")[0]
        self.getTagList()
        search_str = "(HEAD, tag: "
        self.logger.info("des_line=" + des_line)
        if search_str in des_line and ")" in des_line:
            left_loc = des_linex.index(search_str)
            right_loc = des_linex.rindex(")")
            tag_name = des_linex[left_loc + len(search_str):right_loc]
            self.logger.info("tag_name=" + tag_name)
            if tag_name in self.tagList:
                return tag_name
        return ""
    
    def getDescriptionByCommitID(self, commitID):
        tagName = self.getTagBycommitID(commitID)        
        p = subprocess.Popen(["git", "show", commitID], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        #show 8 lines
        descriptions = out.decode('utf-8').split("\n")
        ret_des = ""
        if tagName:
            ret_des = commitID + "(tag:" + tagName + ")\n"        
        for index, des_item in enumerate(descriptions):
            if index >= 8:
                break
            ret_des = ret_des + des_item + "\n"                
        self.logger.info("git show" + commitID)
        self.logger.info("ret_des:" + ret_des)
        self.logger.info("stderr:" + error.decode('utf-8'))
        self.logger.info("return code:" + str(p.returncode))        
        return ret_des
        
    def getLatest10commitID(self, branchname):
        if not branchname:
            return ""
        p = subprocess.Popen(["git", "rev-list", "-10", branchname], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, error = p.communicate()
        outlines = out.decode('utf-8').strip().split("\n")
        self.logger.info("git rev-list -10 " + branchname)
        self.latestCommitIDList[branchname] = []
        for outline in outlines:
            outline = outline.strip()
            if outline:
                self.logger.info("outline:" + outline)                     
                self.latestCommitIDList[branchname].append(outline)
        self.logger.info("stderr:" + error.decode('utf-8'))
        self.logger.info("return code:" + str(p.returncode))
        return self.latestCommitIDList[branchname]
    
    def populateCommitIDByTag(self):
        if not self.tagList:
            self.getTagList()
        self.logger.info("tagList=\n" + str(self.tagList))
        self.commitIDbyTag = {}
        for index_tag, tag in enumerate(self.tagList):
            #for time saving only parse latest 10 tags
            if index_tag > 10:
                break
            p = subprocess.Popen(["git", "show", tag], cwd=self.wdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, error = p.communicate()
            outlines = out.decode('utf-8').strip().split("\n")
            if tag=="ES2_DEV_V0.1_2020-04-02":
                self.logger.info("outlines:\n")
                self.logger.info(out.decode('utf-8').strip())
            for line in outlines:                
                if "commit" in line:
                    linesplits = line.strip().split(" ")
                    if len(linesplits) == 2:
                        commitID = line.strip().split(" ")[1]
                        self.logger.info("line=" + line.strip())
                        self.logger.info( str(line.strip().split(" ")) )
                        self.logger.info("commitID=" + commitID)
                        self.commitIDbyTag[tag] = commitID                    
                    else:
                        continue
                else:
                    continue
        
        self.logger.info("commitIDbyTag:\n" + str(self.commitIDbyTag))
        
    def getTagBycommitID(self, CommitID):
        if not self.commitIDbyTag:
            self.populateCommitIDByTag()            
        
        self.logger.info("CommitID=" + CommitID) 
        self.logger.info("self.commitIDbyTag")
        self.logger.info(str(self.commitIDbyTag))
        for key in self.commitIDbyTag:
            self.logger.info(str(key))
            
        self.logger.info(str(type(self.commitIDbyTag))) 
        for value in self.commitIDbyTag.values():
            self.logger.info(str(value))
        
        for key, val in self.commitIDbyTag.items():            
            if CommitID == val:
                return key        
        
        self.logger.info("End") 
        return ""
        
    def getCommitIDbyTag(self, Tag):
        if not self.commitIDbyTag:
            self.populateCommitIDByTag()            
        
        self.logger.info("Tag=" + Tag) 
        self.logger.info("self.commitIDbyTag")
        self.logger.info(str(self.commitIDbyTag))
        for key, value in self.commitIDbyTag.items():
            if Tag == key:
                return value        
        return ""
        
    #for select branch/commit page display
    def getAvailableCommitList(self, branchname):
        listOfCommit = []
        self.logger.info("branchname=" + branchname)
        curID = self.currentCommitID()
        if not curID:
            listOfCommit.append({"":""})
        self.logger.info("curID=" + curID)
        if not branchname:
            self.getTagList()
            for tag in self.tagList:
                commitID = self.getCommitIDbyTag(tag)
                desc = self.getDescriptionByCommitID(commitID)
                this_commit = commitID + "(tag:" + tag + ")"
                if curID == commitID:
                    listOfCommit.insert(0, {this_commit:desc})
                else:
                    listOfCommit.append({this_commit:desc})
            
            self.logger.info("listOfCommit")
            
            for onecommit in listOfCommit:
                self.logger.info(str(onecommit))
                for key, value in onecommit.items():
                    self.logger.info("key:\n")
                    self.logger.info(key)
                    self.logger.info("value:\n")
                    self.logger.info(value)
            return listOfCommit
                
        if not self.latestCommitIDList or \
             not branchname in self.latestCommitIDList:
            self.getLatest10commitID(branchname)
            
        self.logger.info("latestCommitIDList " + branchname)    
        self.logger.info(str(self.latestCommitIDList[branchname]))    
        for commitID in self.latestCommitIDList[branchname]:
            desc = self.getDescriptionByCommitID(commitID)
            this_commit = ""
            tagname = self.getTagBycommitID(commitID)
            if tagname:
                this_commit = commitID + "(tag:" + tagname + ")"
            else:
                this_commit = commitID
            if curID == commitID:
                listOfCommit.insert(0, {this_commit:desc})
            else:
                listOfCommit.append({this_commit:desc})
        
        for onecommit in listOfCommit:
            for key, value in onecommit.items():
                self.logger.info("key=" + key)
                self.logger.info("value=" + value)
                
        return listOfCommit
    
if __name__ == '__main__':
    myrepo = repositoryHelper(sys.argv[1], sys.argv[2])  