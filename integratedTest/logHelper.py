import os
import sys
import traceback
import datetime
import shutil  

class logHelper(object):
    def __init__(self, repfilename, suite_name, instance_name, logRoot):
        self.terminal = sys.stdout
        self.logRoot = logRoot
        self.instance_name = instance_name
        self.suite_name = suite_name
        self.report = repfilename
        try:
            if not os.path.isdir(self.logRoot): 
                raise OSError("Error logRoot not exist!")
            self.dir_path = os.path.join(self.logRoot, suite_name)
            if not os.path.isdir(self.dir_path):
                os.mkdir(self.dir_path)
            self.dir_path = os.path.join(self.dir_path, instance_name)
            if not os.path.isdir(self.dir_path):
                os.mkdir(self.dir_path)
        except OSError as err:
            raise(Exception(str(traceback.print_exc())))
            
        
        self.base_name = os.path.basename(repfilename)        
        if "." in self.base_name:
            ind = self.base_name.rindex(".")
            self.base_name = self.base_name[:ind]
        print("logHelper base_name", self.base_name) 
        print("logHelper dir_path", self.dir_path)
        self.full_name = os.path.join(self.dir_path, self.base_name + ".log")
        print("logHelper init full_name:", self.full_name)
        self.report = self.report.replace("json", "log")
        
    def write(self, message = ""):
        if not message:
            return
        #msg = str(datetime.datetime.now()).replace(" ", "_").replace(r".", "_").replace(r":", "-") + ":\t" + message
        self.terminal.write(message)        
        try:
            with open(self.full_name, "a") as outfile:
                outfile.write(message)            
        except OSError:
            traceback.print_exc()
                    
        
    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass 
        
    def getLogCotent(self):
        if not os.path.isfile(self.full_name):
            return "The log file not exist:" + self.full_name + "\nPlease check again."
        try:
            with open(self.full_name, "r") as outfile:
                return outfile.readlines()
        except OSError:
            return str(traceback.print_exc())
        
        return ""
        
    def getLogFilePath(self):
        return self.full_name
    
    def getLogInReport(self):
        return self.report 
    
    def saveLogToReport(self):
        report_path = ""
        report_dir = self.report
        if self.report.endswith(os.path.sep):
            report_dir = self.report[:-1]        
        report_path = os.path.dirname(report_dir)
        print("logHelper saveLogToReport full_name:", self.full_name)
        print("logHelper saveLogToReport src:", self.full_name)
        print("logHelper saveLogToReport report_path:", report_path)
        shutil.copy(self.full_name, report_path )
        
if __name__ == '__main__':
    mylog = logHelper(sys.argv[1])  
    
    