import os
import sys
import traceback
import json
import platform
import datetime
import subprocess
from .repositoryHelper import repositoryHelper 

from pathlib import Path
  
#forWrite=True if you are creating a new report

class reportHelper():
    def __init__(self, suite_name, instance_name, reportRoot, logger, forWrite=True):       
        self.reportRoot = reportRoot
        self.rep_path = ""
        self.instance_name = instance_name
        self.suite_name = suite_name
        self.fullpath_report = ""
        self.reportContent = { 
        "instance_name":self.instance_name if instance_name else "Name not provided",
        "suite_name":self.suite_name if suite_name else "Name not provided"}
        self.current_case_result = [] 
        self.current_case_meta = {}
        self.current_case = {}
        self.case_results = []
        self.inst_dir = ""
        self.isTrueRun = True
        if not logger:
            return None        
        self.logger = logger
        try:            
            #self.logger.info("reportHelper:reportRoot:" + self.reportRoot)
            if not os.path.isdir(self.reportRoot): 
                raise OSError("Error reportRoot not exist!")
                
            self.rep_path = os.path.join(self.reportRoot, self.suite_name)            
            if not os.path.isdir(self.rep_path):
                self.createFolder(self.rep_path)
                
            self.rep_path = os.path.join(self.rep_path, self.instance_name, "")
            if not os.path.isdir(self.rep_path):
                self.createFolder(self.rep_path)            
            
            self.logger.info("reportHelper:rep_path:" + self.rep_path)
            if forWrite:
                self.dirname = "instance_" + self.instance_name + "__" + \
                str(datetime.datetime.now()).replace(" ", "_").replace(r".", "_").replace(r":", "-")            
                self.inst_dir = os.path.join( self.rep_path, self.dirname)
                self.logger.info("reportHelper:reportHelper init: inst_dir:" + self.inst_dir)
                if not os.path.isdir(self.inst_dir):
                    os.mkdir(self.inst_dir)                
                self.fullpath_report = os.path.join(self.inst_dir, self.dirname + ".json")
                self.logger.info("reportHelper:fullpath_report:" + self.fullpath_report)
            
        except:
            self.logger.error(str(traceback.print_exc()) )            
            raise(Exception(str(traceback.print_exc())))
        
    def createFolder(self, path):
        try:
            if not os.path.isdir(path):
                os.mkdir(path)
                if not os.path.isdir(path):
                    raise(Exception("Can't create dir for " + path))
        except OSError as err:
            raise(Exception(str(err)))
               
    #self.current_case={
    #    "name":casename, #string, case uniquely, may run multiple times here
    #    "meta":self.current_case_meta, #dictionary, meta data like gitserver etc., case uniquely
    #    "results":self.current_case_result} 
    #"results":array of results, 1 element for 1 round of case running
    #testMe instance is object of test suite(a template); instance is organized like:
    #case1*n1 times + case2*n2 times ....., here current_case save information of each casei*ni times 
    #newCaseMeta case1*n1 times + newCaseMeta case2*n2 times...+ newCaseMeta casei*ni times
    def newCaseMeta(self, casename, case_meta):
        if self.current_case:
            self.current_case["results"] = self.current_case_result
            self.case_results.append(self.current_case)
            self.current_case = {}
            self.current_case_result = [] 
            self.current_case_meta = {}
            
        self.current_case_meta = case_meta
        self.current_case["name"] = casename
        self.current_case["meta"] = self.current_case_meta        
        self.current_case_result = []
        
        
    def addCaseMeta( self, case_meta ):
        self.current_case_meta.update(case_meta)
        self.logger.info("reportHelper addCaseMeta:\n")
        self.logger.info(str(self.current_case_meta))
        self.logger.info("\n")
        
    def addCaseResult(self, round, pass_or_fail, err_msg, start_t, end_t):
        one_result = {
            "round":round,
            "p_f":pass_or_fail,
            "msg":err_msg,
            "start": str(start_t),
            "end": str(end_t),
            "dur": str(end_t - start_t)
        }
        self.current_case_result.append( one_result )
        
    def addGlobals(self, data_in_hash ):
        self.reportContent.update(data_in_hash)
        self.logger.info(str(self.reportContent))
    
    def setTrueRun(self, isTrueRun = True):
        self.isTrueRun = isTrueRun
        
    def writeReport(self, project_name, repos_root, config_info): 
        
        try:
            self.current_case["results"] = self.current_case_result
            if self.current_case:
                self.case_results.append(self.current_case)          
            
            self.reportContent["test_cases"] = self.case_results
            
            with open(self.fullpath_report, 'w') as outfile:
                json.dump(self.reportContent, outfile)
            
            #save env:python modules/versions; disable when FakeRun for quick responding
            if self.isTrueRun:
                py_module_list_file = os.path.join(self.inst_dir, "py_module_list.txt")
                p = subprocess.Popen(["pip", "list"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)    
                file_content = []
                self.logger.info("reportHelper:pip list:")
                while p.poll() is None:                
                    stdoutline = str(p.stdout.readline())
                    #self.logger.info(stdoutline)
                    if stdoutline:
                        file_content.append(stdoutline)
                if stdoutline:
                    with open(py_module_list_file, 'w') as pylistfile:
                        self.logger.info("reportHelper:pip list saving:")
                        pylistfile.writelines("\n".join(file_content))
            '''
            if config_info["have_config_file_as_input"]:
                if os.path.isfile(config_info["file_path"]):
                    self.logger.info("reportHelper:Keep config file for reproduce.")
                    self.logger.info(str(config_info["have_config_file_as_input"]))
                    self.logger.info(config_info["file_path"])
                    shutil.copy(config_info["file_path"], self.inst_dir)
            '''    
        except Exception as err:
            raise err
            
    def getReportName(self):
        return self.fullpath_report
    
    def getReportDir(self):
        return self.inst_dir
    
    def getInsReportRoot(self):
        return self.rep_path
        
    def readReport(self, filename_in_search="", report_time = ""):
        try:
            reportOutContent={}
            allReports=[]
            filenamelist=[]
            reportOutContent["rep_requested"] = {}
            reportOutContent["fname_to_display"] = ""
            listOfFile = os.listdir(self.rep_path)
            #self.logger.info("reportHelper:readReport rep_path:" + self.rep_path)
            #self.logger.info("reportHelper:readReport filename_in_search:" + filename_in_search)
            currentFolder = ""
            currentFolderLast = ""
            for item_name in listOfFile:
                runningFolder = os.path.join(self.rep_path, item_name)
                #self.logger.info("readReport runningFolder:" + runningFolder)
                if os.path.isdir(runningFolder) and \
                    item_name.startswith( "instance_" + self.instance_name + "__" ):                
                    listOfRunningFolder = os.listdir(runningFolder)
                    #self.logger.info("readReport listOfRunningFolder:" + str(listOfRunningFolder))
                    for file_name in listOfRunningFolder:
                        candidate_fname = os.path.join(runningFolder, file_name)
                        #self.logger.info("readReport candidate_fname:" + candidate_fname)
                        if os.path.isfile(candidate_fname) and \
                            file_name.startswith( "instance_" + self.instance_name + "__" )\
                            and file_name.endswith("json"):
                            #self.logger.info("reportHelper:report file found:", file_name)
                            with open( candidate_fname ) as f:
                                oneReport = json.load(f)
                                '''  
                                oneReport={
                                    "case_list":json_content["test_cases"],
                                    "instance_name":json_content["instance_name"],
                                    "suite_name":json_content["suite_name"],
                                }'''
                                filenamelist.append(item_name)                    
                                allReports.append(oneReport)
                                currentFolderLast = runningFolder
                                #self.logger.info("reportHelper:\nfilename_in_search:" + filename_in_search)
                                #self.logger.info("reportHelper:\nitem_name:" + item_name)
                                #self.logger.info("reportHelper:\nfile_name:" + file_name)
                                if filename_in_search and item_name == filename_in_search:
                                    reportOutContent["rep_requested"] = oneReport
                                    reportOutContent["fname_to_display"] = filename_in_search
                                    currentFolder = runningFolder
                            
            #reportOutContent["reports"]=allReports 
            new_filenamelist = sorted(filenamelist, reverse=True)
            if not reportOutContent["rep_requested"] and len(allReports)>=1:
                reportOutContent["rep_requested"] = allReports[len(allReports)-1]
                reportOutContent["fname_to_display"] = new_filenamelist[0]
                #self.logger.info("rep_requested not set, get the last one:" + str(reportOutContent["rep_requested"]) )
                #self.logger.info("fname_to_display:" + str(reportOutContent["fname_to_display"]))
            reportOutContent["filenamelist"]=new_filenamelist
            if currentFolder:
                reportOutContent["currentFolder"]=currentFolder
            else:
                reportOutContent["currentFolder"]=currentFolderLast
        except:            
            raise Exception(str(traceback.print_exc()))
            
        return reportOutContent
        
    
    
if __name__ == '__main__':
    myExtract = reportHelper(sys.argv[1])  