#py unit_test_main.py D:\path\to\pdk py pdk
#py unit_test_main.py D:\path\to\roma py roma
#with support C later
#py unit_test_main.py D:\project\local_play\code_change py

import importlib
import traceback
import sys
import platform
import unittest.mock
import os
import json
import csv

sys.path.append("..\\..\\stub")

from scanPythonFile import CApiExtracter

OS_slash = "\\"
if platform.system() == "Windows": 
    OS_slash = "\\"
else:
    OS_slash = "/"

sys.path.append(".."+ OS_slash +"test_environment"+ OS_slash +"stub")

class CUnitTest():
    def __init__(self, arg_list):
        self.arg_list = arg_list
        self.type = r".py"
        self.file_list = []
        self.total_api = 0
        self.config_global = {}
        self.test_settings = {}
        self.api_list = {}
        self.api_list["test_suit"] = []
        self.repo_path = ""        
        self.current_suite_name = "pdk"
        self.suite = {}
        self.api_list_filename = r"api_full_list.csv"       
        self.api_to_do_list_filename = r"api_to_test.json" 
        self.todo_list = []
        
    def parseConfig(self):
        with open('config.json', "r") as json_file:
            self.config_global = json.load(json_file)
            if self.config_global["test_type"] != "Unit":
                raise Exception("the config.json file test_type not correct:" + self.config_global["test_type"])
            self.test_settings = self.config_global["test_settings"]
        if len(self.arg_list)>1 and self.arg_list[1]:
            self.repo_path = self.arg_list[1]        
        else:            
            self.repo_path = self.test_settings["repo_path"]
        
        print("repo_path", self.repo_path)
        if self.repo_path and "roma" in self.repo_path.lower():            
            self.current_suite_name = "roma"
            print("current_suite_name 1", self.current_suite_name)
        else:            
            self.current_suite_name = "pdk"   
                   
        print("current_suite_name 2", self.current_suite_name)
        
    def run(self):
        try:
            self.parseConfig()
            #optional
            if self.repo_path and not self.test_settings["do_not_list_API"]:
                self.populate_API_list()
            if self.test_settings["list_API_only"]:
                return
            
            with open(self.api_to_do_list_filename, 'r') as outfile:
                new_data = json.load(outfile) 
                self.todo_list = new_data["to_do_list"]
                #compare
            print( "to do list:" + str(self.todo_list) + "\n" )
            
            for test_item in self.todo_list: 
                if test_item["skip"]:
                    print(test_item["api"]+" skipped")
                    continue
                else:
                    print(test_item["api"])
                    
                self.module_name = ""  
                self.path = ""                
                path_module = test_item["path_and_name"]              
                
                if OS_slash in path_module:
                    loc = path_module.rindex(OS_slash)
                    if loc >= 0:
                        self.path = path_module[0:loc]
                        self.module_name = path_module[loc+1:]
                    else:
                        raise ValueError("Unknown error: " + OS_slash + " not in " + path_module)
                else:
                    self.module_name = path_module  
                if "." in self.module_name:
                    loc = self.module_name.rindex(".")
                    if loc >= 0:
                        self.module_name = self.module_name[0:loc]                    
                
                print("self.path:" + self.path + ";\tself.module_name:" + self.module_name + "\n")
                if self.path:
                    sys.path.append(self.path)
                    
                if self.module_name:                    
                    self.mymodule = importlib.import_module(self.module_name)
                else:
                    raise ValueError("argument error: python file name empty!")
                
                api_name = test_item["api"] 
                #Note: get_file_and_dir_list is a first example for unit test. Presumably we know more details of its calling
                parameter_list = test_item["parameters"]
                return_list_str = test_item["returns"]
                return_list = []
                for curr_para in parameter_list:
                    print( "curr_para:" + str(curr_para) )                    
                    return_list = getattr(self.mymodule, api_name)( *curr_para )
                    index = 1
                    for item in return_list:
                        print( "return_list item %d "%index + str(return_list_str[index-1]) + ":" +str(item) )
                        print( "\n" )
                        index = index +1
                
        except:
            traceback.print_exc()

    def populate_API_list(self):           
        if not os.path.exists( self.repo_path ):
            raise Exception("Folder to be tested doesn't exist:" + self.repo_path)
        if os.path.isfile(self.repo_path):
            #leave it later
            raise Exception("The path is a file. Need a folder instead:" + self.repo_path)
        
        #get all files's name to be tested            
        for (dirpath, dirnames, filenames) in os.walk(self.repo_path): 
            dir_skip = False
            for item in self.test_settings["except_folder"]:
                if not item in dirpath:
                    continue
                else:
                    #print("except:", dirpath)
                    dir_skip = True
                    break
            if dir_skip:
                continue
            for filename in filenames:                
                if filename.endswith(self.type) and not filename in self.test_settings["except_file"]: 
                    #print("out filename:" + filename)
                    self.file_list.append(os.path.join(dirpath, filename))
        
        index_of_file = 0
        for py_file in self.file_list:            
            index_of_file = index_of_file + 1
            print("\nfile %3d"%index_of_file + ":" + py_file )
            myExtract = CApiExtracter(py_file, self.test_settings["except_api"])                
            self.total_api = self.total_api + myExtract.get_total()
            self.suite[py_file] = myExtract.get_list()
            
        with open(self.api_list_filename, 'w') as outfile:
            new_line = [self.current_suite_name, "filename(.py)","api name","description"]
            writer = csv.writer(outfile)
            writer.writerow(new_line)
            for key in self.suite.keys():
                stripped_key = key
                if "\\" in key:
                    loc = key.rindex("\\")
                    stripped_key = key[loc+1:]
                    #print("stripped_key 1:"+stripped_key)
                    if stripped_key.endswith(r".py"):
                        loc = stripped_key.rindex(r".")
                        stripped_key = stripped_key[:loc]
                        #print("stripped_key 2:"+stripped_key, ";loc:"+str(loc))
                new_line = ["", str(stripped_key), str(len(self.suite[key])) + " API(s)", ""]
                writer.writerow(new_line)
                if len(self.suite[key])==0:
                    continue                
                for item in self.suite[key]:
                    stripped_item = item
                    if "(" in item:
                        loc = item.rindex("(")
                        stripped_item = item[:loc]
                    new_line = ["", "", str(stripped_item), ""]
                    writer.writerow(new_line)
                
        
        print("\n\nIn total, %d"%self.total_api + " APIs will be tested in " + self.repo_path )
    
#first edition of unit test
#sys.argv[1]: repo path default="."
#sys.argv[2]: file type default="py", current supported

if __name__ == '__main__':    
    print("Argument list:" + str( len(sys.argv) ) )
    if len(sys.argv) == 2: 
        if sys.argv[1] == "-h" or sys.argv[1] == "-help":
            print("Example(Windows or Linux):")
            print(r"py unit_test_main.py /path/to/pdk py pdk")
            print(r"py unit_test_main.py /path/to/roma py roma")
            sys.exit(0)
        elif sys.argv[1] == "-v" or sys.argv[1] == "-version":
            print("Chaoyang unit test tool version 0.1")
            sys.exit(0)
    
    print("Argument list:" + str( sys.argv ) )
            
    if len(sys.argv) >= 3 and (sys.argv[2].strip().lower() != "py" and sys.argv[2].strip().lower() != "python"):
        #print("%d"%len(sys.argv) + sys.argv[2].strip().lower() )
        print("Can test python file only at the moment.")
        sys.exit(0)
        
    myTest = CUnitTest(sys.argv)
    myTest.run()
    print("test completed!" )
  
  
  
  