import os
import sys
import traceback
import json
import platform
import datetime
import shutil
from pathlib import Path
from .models import Projects  
from .models import ActiveTestSet, get_obj_else_None

'''
    repositoryRoot:absolute path of test_root\test_repo
    scriptRoot:absolute path of test_root\test_scripts
    logRoot:absolute path of test_root\test_log
    reportRoot:absolute path of test_root\test_report
    More description:
    All folders have subfolder for every project, PDK or ROMA or any others
    folderHelper will check/create if not-exist for every starting.
    test_repo:project repository by project
    test_scripts:user test scripts by project
    test_log:test log files by project 
    test_report:test reports by project
'''
class folderHelper():
    def __init__(self, logger=None):       
        self.repositoryRoot = ""
        self.repoRootHash={}
        self.repoPPCHash={}
        self.scriptRoot = ""
        self.scriptRootHash={}
        self.logRoot = ""
        self.logRootHash={}
        self.reportRoot = ""
        self.reportRootHash={}
        self.resourceRoot = ""
        self.resourceRootHash={}
        self.pro_list = {}
        self.hasError = False
        if not logger:
            return None        
        self.logger = logger
        
        try:
            proj_obs = Projects.objects.all()
            if not proj_obs or len(proj_obs) == 0:
                err = "Add your first project please."
                raise(Exception(err))
            for item in proj_obs:
                mod_list = item.module_list.split()
                self.pro_list[item.name] = mod_list
            
            directory_path = os.getcwd()
            directory_path = os.path.dirname(os.path.dirname(directory_path))            
            self.repositoryRoot = os.path.join(directory_path, "test_repo")
            self.scriptRoot = os.path.join(directory_path, "test_scripts")
            self.logRoot = os.path.join(directory_path, "test_log")
            self.reportRoot = os.path.join(directory_path, "test_report")
            self.resourceRoot = os.path.join(directory_path, "test_resource")
            
            if not os.path.isdir(self.repositoryRoot):
                self.createFolder(self.repositoryRoot)                
            if not os.path.isdir(self.scriptRoot):
                self.createFolder(self.scriptRoot)
            if not os.path.isdir(self.logRoot):
                self.createFolder(self.logRoot)
            if not os.path.isdir(self.reportRoot):
                self.createFolder(self.reportRoot)  
            if not os.path.isdir(self.resourceRoot):
                self.createFolder(self.resourceRoot)
                
            if self.pro_list:
                for pro_name, mods in self.pro_list.items():
                    self.logger.info("pro_name:" + pro_name )
                    rep_proj_path = os.path.join(self.repositoryRoot, pro_name)
                    self.repoRootHash[pro_name] = rep_proj_path
                    ppc_path = os.path.join(rep_proj_path, "ppc")
                    self.repoPPCHash[pro_name] = ppc_path
                    
                    script_proj_path = os.path.join(self.scriptRoot, pro_name)
                    self.scriptRootHash[pro_name] = script_proj_path
                    
                    log_proj_path = os.path.join(self.logRoot, pro_name)
                    self.logRootHash[pro_name] = log_proj_path
                    
                    report_proj_path = os.path.join(self.reportRoot, pro_name)
                    self.reportRootHash[pro_name] = report_proj_path
                    
                    resource_proj_path = os.path.join(self.resourceRoot, pro_name)
                    self.resourceRootHash[pro_name] = resource_proj_path
                    if pro_name.startswith("ROMA"):
                        int_path = os.path.join(resource_proj_path, "integrated_test")
                    
                    if not os.path.isdir(rep_proj_path):
                        self.createFolder(rep_proj_path)
                    if not os.path.isdir(ppc_path):
                        self.createFolder(ppc_path) 
                        
                    if not os.path.isdir(script_proj_path):
                        self.createFolder(script_proj_path)
                        
                    if not os.path.isdir(log_proj_path):
                        self.createFolder(log_proj_path)
                        
                    if not os.path.isdir(report_proj_path):
                        self.createFolder(report_proj_path)
                        
                    if not os.path.isdir(resource_proj_path):
                        self.createFolder(resource_proj_path)
                    if pro_name.startswith("ROMA"):
                        if not os.path.isdir(int_path):
                            self.createFolder(int_path)
                    for module_name in mods:
                        resource_module_path = os.path.join(int_path, module_name)
                        if not os.path.isdir(resource_module_path):
                            self.createFolder(resource_module_path)
                        #create default test set for every module
                        default_ts_path = os.path.join(resource_module_path, "ts_default")
                        if not os.path.isdir(default_ts_path):
                            self.createFolder(default_ts_path)
                        if pro_name == "ROMA_AI":
                            config_path = os.path.join(rep_proj_path, "arch-exp-scripts")
                            config_path = os.path.join(config_path, "config")
                            config_path = os.path.join(config_path, "ai_e2e_config.yml")
                            if not os.path.isfile(config_path):
                                self.logger.error("config_path not valid:"+config_path)
                            else:
                                shutil.copy(config_path, \
                                os.path.join(default_ts_path, "ai_e2e_config.yml") )
            else:
                self.logger.warning("Project name not defined.")
                        
            
        except OSError as err:
            self.logger.error("error:", err ) 
            self.logger.error("traceback info:", str(traceback.print_exc()) )
            self.hasError = True           
            return False        

        #self.print_all()
        
    def get_reposit(self, projname):
        if not projname in self.repoRootHash:
            self.logger.info("self.repoRootHash:" + str(self.repoRootHash) )
            raise(Exception("Folder of " + projname + " not found!"))
        else:
            return self.repoRootHash[projname]
    
    def get_script(self, projname):
        if not projname in self.scriptRootHash:
            self.logger.info("self.scriptRootHash:" + str(self.scriptRootHash) )
            raise(Exception("Folder of " + projname + " not found!"))
        else:
            return self.scriptRootHash[projname]        
    
    def get_log(self, projname):
        if not projname in self.logRootHash:
            self.logger.info("self.logRootHash:" + str(self.logRootHash) )
            raise(Exception("Folder of " + projname + " not found!"))
        else:
            return self.logRootHash[projname]        
    
    def get_report(self, projname):
        if not projname in self.reportRootHash:
            self.logger.info("self.reportRootHash:" + str(self.reportRootHash) )
            raise(Exception("Folder of " + projname + " not found!"))
        else:
            return self.reportRootHash[projname]
            
    def get_resource(self, projname):
        if not projname in self.resourceRootHash:
            self.logger.info("self.resourceRootHash:" + str(self.resourceRootHash) )
            raise(Exception("Folder of " + projname + " not found!"))
        else:
            return self.resourceRootHash[projname]
            
    def get_all(self, projname) :
        if not projname in self.repoRootHash or not projname in self.scriptRootHash or \
        not projname in self.logRootHash or not projname in self.reportRootHash:            
            raise(Exception("Folder of " + projname + " not found!"))
        else:
            return self.repoRootHash[projname], self.scriptRootHash[projname],\
            self.logRootHash[projname], self.reportRootHash[projname],\
            self.resourceRootHash[projname]        
        
    def print_all(self):
        self.logger.info("repositoryRoot:" + self.repositoryRoot)   
        self.logger.info("scriptRoot:" + self.scriptRoot)
        self.logger.info("logRoot:" + self.logRoot)
        self.logger.info("reportRoot:" + self.reportRoot)
        self.logger.info(str(self.repoRootHash))
        self.logger.info(str(self.scriptRootHash))
        self.logger.info(str(self.logRootHash))
        self.logger.info(str(self.reportRootHash))
        self.logger.info(str(self.resourceRootHash))
            
    def createFolder(self, path):
        try:
            if not os.path.isdir(path):
                os.mkdir(path)
                if not os.path.isdir(path):
                    raise(Exception("Can't create dir for " + path ))
        except OSError:
            pass
        
    def getTsDir(self, resource_root, t_type, mod_name, inst_id, ind):
        ret_path = os.path.join(resource_root, t_type)
        if mod_name:
            ret_path = os.path.join(ret_path, mod_name)
            if not os.path.isdir(ret_path):
                os.mkdir(ret_path)
        else:
            raise OSError("Error: case_i.module_name is empty:" + mod_name)
        ts_object = get_obj_else_None(ActiveTestSet, 
            instance_id = inst_id, case_index_in_instance = ind)
        if not ts_object:
            self.logger.info("Error: TestSet not found in database")
            return ""
        else:
            ret_path = os.path.join(ret_path, ts_object.name)
            self.logger.info("test set used:" + ret_path)
        return ret_path
    
if __name__ == '__main__':
    my_repo = repoHelper(sys.argv[1])  